//
//  ActionViewController.swift
//  RehostUpload
//
//  Created by FLK on 11/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
import MobileCoreServices


class ActionViewController: UIViewController {

  @IBOutlet weak var imageView: UIImageView!

  override func viewDidLoad() {
    super.viewDidLoad()
    print("viewDidLoad")
    // Get the item[s] we're handling from the extension context.

    // For example, look for an image and place it into an image view.
    // Replace this with something appropriate for the type[s] your extension supports.
    var imageFound = false

    for item in self.extensionContext!.inputItems as! [NSExtensionItem] {
      for provider in item.attachments! as! [NSItemProvider] {
        if provider.hasItemConformingToTypeIdentifier(kUTTypeImage as String) {
          // This is an image. We'll load it, then place it in our image view.


          weak var weakImageView = self.imageView
          provider.loadItem(forTypeIdentifier: kUTTypeImage as String, options: nil, completionHandler: { (item, error) in

            OperationQueue.main.addOperation {
              if let strongImageView = weakImageView {
                if let imageURL = item as? URL {
                  
                  if let image = UIImage(data: try! Data(contentsOf: imageURL)) {
                    self.uploadSelectedImage(image: image)
                    strongImageView.image = image
                  }
                } else if let image = item as? UIImage {
                  self.uploadSelectedImage(image: image)
                  strongImageView.image = image
                }
              }
            }
          })

          imageFound = true
          break
        }
      }

      if (imageFound) {
        // We only handle one image, so stop looking for more.
        break
      }
    }

  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  func uploadSelectedImage(image: UIImage) {
    RehostKit.client.upload(image: image).done { rehostImage in

      print(rehostImage)
      let alert = UIAlertController(title: "reho.st", message: "Image uploadée avec succès !", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Copier le lien dans le presse-papier :o", style: .default) { alert in
        UIPasteboard.general.string = rehostImage.link_preview

      })
      self.present(alert, animated: true)

      }.catch { error in
        print(error)
    }
  }
  @IBAction func done() {
    // Return any edited content to the host app.
    // This template doesn't do anything, so we just echo the passed in items.
    self.extensionContext!.completeRequest(returningItems: self.extensionContext!.inputItems, completionHandler: nil)
  }

  @IBAction func test() {
    // Return any edited content to the host app.
    // This template doesn't do anything, so we just echo the passed in items.
    let alert = UIAlertController(title: "Debug", message: "Hey o/", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK \\o", style: .default))
    self.present(alert, animated: true)
    return
    var string = "HELLO|"
    for item in self.extensionContext!.inputItems as! [NSExtensionItem] {
      for provider in item.attachments! as! [NSItemProvider] {
        if provider.hasItemConformingToTypeIdentifier("public.image") {
          string += String(describing: provider)
          string += " | "
          provider.loadItem(forTypeIdentifier: "public.image", options: nil, completionHandler: { (item, error) in
            string += "qwd1 | \(item)"
            let alert = UIAlertController(title: "Debug", message: string, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true)
          })

        }
      }
    }

    //let alert = UIAlertController(title: "Debug", message: string, preferredStyle: .alert)
    //alert.addAction(UIAlertAction(title: "OK", style: .default))
    //self.present(alert, animated: true)
  }

}
