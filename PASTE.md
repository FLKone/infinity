#  How to determine why WKWebView is crashing
```
kvoController?.observe(webView, keyPath: "URL", options: NSKeyValueObservingOptions.New|NSKeyValueObservingOptions.Old) { (areaViewController, webView, change) -> Void in
  if change[NSKeyValueChangeNewKey] is NSNull && !(change[NSKeyValueChangeOldKey] is NSNull) {
    areaViewController.setup() // reload our webview
  }
}
```
> https://stackoverflow.com/questions/31994919/how-to-determine-why-wkwebview-is-crashing


# WKWebView Leak

```
class LeakAvoider : NSObject, WKScriptMessageHandler {
weak var delegate : WKScriptMessageHandler?
init(delegate:WKScriptMessageHandler) {
self.delegate = delegate
super.init()
}
func userContentController(userContentController: WKUserContentController,
didReceiveScriptMessage message: WKScriptMessage) {
self.delegate?.userContentController(
userContentController, didReceiveScriptMessage: message)
}
}
```
```
self.wv.configuration.userContentController.addScriptMessageHandler(
LeakAvoider(delegate:self), name: "dummy")
```
```
deinit {
println("dealloc")
self.wv.stopLoading()
self.wv.configuration.userContentController.removeScriptMessageHandlerForName("dummy")
}
```

> https://stackoverflow.com/questions/26383031/wkwebview-causes-my-view-controller-to-leak


# Resize

http://vocaro.com/trevor/blog/2009/10/12/resize-a-uiimage-the-right-way/comment-page-3/
