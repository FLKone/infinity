## Blabla
J'ai ajouté une feuille de route (cf. tout en bas), l'objectif de la 0.0.1 est de  recuperer/afficher tout ce qui à un intérêt (que ça soit présent dans la v1 ou non)
La TODO liste donc tout ce que j'ai repéré, s'il en manque > MP
Si quelque chose est possible sur la version WEB et que ça ne l'est pas sur cette v2 > MP

Après cette étape pas très sexy on passera aux choses intéressantes ;)

## TODO
+ Blacklist
+ Afficher notes de version si maj
+ Rehost Intégration IN app, sauvegarde liste, acces tous les liens, écran de chargement, gestion des erreurs


## TODO v0.0.1
- Global: Pagination (liste de topics, liste de MP & liste des messages)
- [#24] Topics: Supprimer flag du topic
- [#19] Listes: Afficher statut du topic (Epinglé, Fermé)
- [#21] Listes: Afficher statut du topic (Lu/Non lu, etc.)
- [#21] Listes: Afficher toutes les infos des sujets (Nb réponses, destinataire, date dernière réponse etc.)
- [#21] Listes: Accès direct à un topic via page spécifique (Première/Dernière/Derniere Message)
- [#24] Listes: Accès direct à un topic via page spécifique (Saisie d'un numéro de page)
- [#21] Listes: Accès au lien de la page 1 du topic
- [#24] Catégories: Les sous-categories
- [#27] Catégories: Choix du type de flag
- Catégories: Créer un nouveau sujet
- [#24] Favoris: Choix du type de flag
- [#24] Favoris: Supprimer flag du topic
- [#19] Favoris: Catégories
- [#19] Messages: Date (ajout/edition)
- [#19] Messages: Nombre de citations (+ Lien)
- Messages: Ton
- [#32] Messages: Ajouter flag
- Messages: Recherche intra (Pseudo & Mot)
- [#30] Messages: Citer (+ Multiple)
- Messages: Supprimer
- Messages: Alerter
- Messages: Lien direct
- Messages: Liens externes
- Messages: Liens internes (Topic, Profiles & autres)
- Messages: Accès Profile + Config
- [#26] Messages: Réponse rapide
- [#19] MP: Statut de lecture
- MP: Supprimer un MP
- MP: Créer un nouveau MP
- MP: Marquer non lu
- [#30] Editeur: Smiley (Base, Perso & Wiki)
- Editeur: Ton du message
- Editeur: Destinataire(s)
- Editeur: Toutes les options (Notifs, signatures, sondages etc.)
- Profil
- Profil: Feedbacks
- Recherche

## Feuille de route
- v0.0.1 : Récuperation de toutes les données nécessaires sur HFR (via la lib MDNKit) & accès/affichage brute dans l'app
- v0.0.2 : Gestion simplifiée du Thème
- v0.0.3 : :o
- v0.0.5 : Mise en place & validation de l'UI/UX définitive
- v0.0.6 : Gestion erreurs/connectivité (Delai depassé, "Ressayer" etc.)
- v0.0.4 : Gestion des options & réglages
- v0.0.7 : :o
- v2.0.0 : Debug -> Store
- v2.0.1 : Profit


## Credits
todo :o
