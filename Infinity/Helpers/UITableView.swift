//
//  UITableView.swift
//  Infinity
//
//  Created by FLK on 30/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

extension UITableView {
  func indexPathForView(view: AnyObject) -> IndexPath? {
    let originInTableView = self.convert(CGPoint.zero, from: (view as! UIView))
    return self.indexPathForRow(at: originInTableView)
  }
}
