//
//  AppDelegate.swift
//  Infinity
//
//  Created by FLK on 07/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  lazy var window: UIWindow? = {
    return UIWindow(frame: UIScreen.main.bounds)
  }()

  func application(_ app: UIApplication, didFinishLaunchingWithOptions options: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    //FIXME Global: Networking issues (Timeout & co)

    MDN.client.configure(url: "https://forum.hardware.fr")
    //MDN.client.logout()

    UINavigationBar.appearance().isTranslucent = false

    let fileManager = FileManager.default
    let urls = fileManager.urls(for: .itemReplacementDirectory, in: .userDomainMask)
    let tmpDir = FileManager.default.temporaryDirectory
    print(tmpDir)
    //if let cachesDirectory = urls.first {
      let avatarsURL = tmpDir.appendingPathComponent("MDN-Avatars", isDirectory: true)
      do {
        try fileManager.createDirectory(at: avatarsURL, withIntermediateDirectories: true, attributes: nil)
      } catch {
        print("error")
      }
      
      do {
        let content = try fileManager.contentsOfDirectory(at: tmpDir, includingPropertiesForKeys: nil, options: [])
        print(content)
      } catch {
        print("error 2")
      }
//        try avatarsURL.checkResourceIsReachable()
      //try avatarsURL?.checkResourceIsReachable() {
      //} else {
        
      //}
    //}
    //self.window?.rootViewController = UINavigationController(rootViewController: GenericListViewController())
    self.window?.rootViewController = MasterViewController()//ThreadSelectionTabBarController()//TabViewController()//
    self.window?.makeKeyAndVisible()

    UIApplication.shared.statusBarStyle = .default
    UIApplication.shared.isStatusBarHidden = false
    /*
    let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
    if statusBar.responds(to: NSSelectorFromString("setBackgroundColor:")) {
      print("sdsd")
      statusBar.backgroundColor = UIColor.black
    }
*/

    let currentVersionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    let currentVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
    let versionOfLastRun = UserDefaults.standard.object(forKey: "VersionOfLastRun") as? String

    if versionOfLastRun == nil {
      // First start after installing the app
      let alert = UIAlertController(title: "Premier lancement !", message: "Hello :o", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "o/ [high five] \\o", style: .default))
      self.window?.rootViewController?.present(alert, animated: true)
    } else if versionOfLastRun != currentVersion {
      // App was updated since last run
      let alert = UIAlertController(title: "Mise à jour :)", message: "HFR+ \(currentVersionNumber ?? "")#\(currentVersion!)", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Voir les changements", style: .default) { _ in
        let md = MarkDownViewController(bundledMarkdown: "CHANGE")
        self.window?.rootViewController?.present(UINavigationController(rootViewController: md), animated: true)
      })
      alert.addAction(UIAlertAction(title: "Cool story bro'", style: .cancel))
      self.window?.rootViewController?.present(alert, animated: true)
    } else {
      // Nothing changed
    }

    UserDefaults.standard.set(currentVersion, forKey: "VersionOfLastRun")
    UserDefaults.standard.synchronize()

    return true
  }

}
