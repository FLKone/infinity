//
//  MDN.swift
//  Infinity
//
//  Created by FLK on 09/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

// swiftlint:disable file_length type_body_length

import Alamofire
import PromiseKit
import Kanna

struct MDNResults<Element> {
    var payload = [Element]()
    var metadata = [String: String]()

    init(payload: [Element], metadata: [String: String]) {
        self.payload = payload
        self.metadata = metadata
    }

    init(payload: [Element]) {
        self.init(payload: payload, metadata: [:])
    }
}

struct MDNSendPostResult {
    var message: String
    var anchor: String?

    init(message: String, anchor: String?) {
        self.message = message
        self.anchor = anchor
    }

}

enum FormMode {
    case creation
    case edition
    case deletion
    case newMessage
}

enum Result<T> {
    case success(T)
    case error(Error)
}

enum MDNException: Error {
    case clientNotConfigured
    case parsing(String)
    case native(String)
    case networkFailure(Error)
    case empty
    case authNeeded
    case invalidCredentials
    case alreadyAlerted
}

enum ThreadType: Int {
    case section = 0
    case starred = 1
    case message = 2

    func defaultPageType() -> ThreadPageType {
        switch self {
        case .section:
            return .firstPage
        case .starred:
            return .lastRead
        case .message:
            return .lastPost
        }
    }

    func defaultFilter() -> SectionFilter {
        switch self {
        case .section:
            return .all
        case .starred:
            return .starred
        case .message:
            return .all
        }
    }

    func filters() -> [SectionFilter] {
        switch self {
        case .section:
            return [.all, .starred, .subscribed, .read]
        case .starred:
            return [.starred, .subscribed]
        case .message:
            return [.all]
        }
    }
}

struct ThreadsResults {
  var sections: [ThreadSection]
  var threads: [[ThreadID: Thread]]
  var form: [String: String]
  var section: Section?
}

struct PostsResults {
  var thread: Thread
  var posts: [PostID: Post]
}

struct PostFormResults {
    var inputs = [String: String]()
    var baseSmileys: [Smiley]?
    var starredSmileys: [Smiley]?
    //FIX ME add submit URL
}

struct AlertFormResults {
    var inputs: [String: String]
    var submitURL: String
}

struct Smiley {
    var code: String
    var url: String
}

class MDN {

    static let client = MDN()

    var configured = false

    /***
     http://forum.judgehype.com/
     http://forum.judgehype.com + /judgehype/Discussionsgenerales/liste_sujet-1.htm
     http://forum.judgehype.com + /judgehype/Discussionsgenerales/topic-voitures-sujet_243861_1.htm
     http://forum.judgehype.com + /login.php?config=judgehype.inc

     http://forum.judgehype.com URL
     http://forum.judgehype.com BASE

     http://forum.hardware.fr/
     http://forum.hardware.fr + /hfr/Hardware/liste_sujet-1.htm
     http://forum.hardware.fr + /hfr/Hardware/2D-3D/unique-amd-vega-sujet_1007921_1.htm
     http://forum.hardware.fr + /login.php?config=hardwarefr.inc

     http://forum.hardware.fr URL
     http://forum.hardware.fr BASE

     http://www.lesnumeriques.com/legrandforum/
     http://www.lesnumeriques.com + /legrandforum/avis/Photo/liste_sujet-1.htm
     http://www.lesnumeriques.com + /legrandforum/avis/Photo/galeries-photos/commenter-photos-avions-sujet_7296_1.htm
     http://www.lesnumeriques.com/legrandforum/ + /login.php?

     http://www.lesnumeriques.com/legrandforum/ URL
     http://www.lesnumeriques.com BASE
     ***/

    private var version: String = "2010.2"
    var forumURL: String?
    var baseURL: String?
    private var config: String?

    private let loginURI = "/login_validation.php"                             // (URL)
    private let favoritesURI = "/forum1f.php?new=0&nojs=0&owntopic="          // (BASE?) //FIXME
    private let messagesURI = "/forum1.php?config=hfr.inc&cat=prive&page=1&owntopic="   // (BASE?) //FIXME
    private let newPostURI = "/bddpost.php"                                    // (URL)
    private let updatePostURI = "/bdd.php"                                     // (URL)
    private let removeStarredURI = "/modo/manageaction.php"
    private let wikiSmileyURI = "/message-smi-mp-aj.php?config=hfr.inc&findsmilies="

    // MARK: Required configuration
    func configure(url: String, base: String) {
        self.forumURL = url
        self.baseURL = base
    }

    func configure(url: String) {
        self.configure(url: url, base: url)
    }

    // MARK: Get/Post requests wrapper
    func getRequest<T>(_ getURI: String, completionBlock: @escaping (_ doc: HTMLDocument) -> Result<T>) -> Promise<T> {
        print("MDNKit > GET REQUEST \(getURI)")
        return Promise { seal in
            guard let fURL = forumURL else {
                seal.reject(MDNException.clientNotConfigured)
                return
            }

            var request = URLRequest(url: URL(string: fURL + getURI)!)
            request.httpMethod = "GET"
            request.timeoutInterval = 10

            Alamofire.request(request).responseString { response in

                switch (response.result) {
                case .success:
                    print("MDNKit > GET REQUEST Success Response for \(getURI)")
                    if let html = response.result.value {

                        if let doc = try? HTML(html: html, encoding: .utf8) {

                            let result = completionBlock(doc)

                            switch result {
                            case .success(let result):
                                seal.fulfill(result)
                            case .error(let err):
                                seal.reject(err)
                            }

                        } else {
                            seal.reject(MDNException.parsing("getRequest #1"))
                        }
                    } else {
                        seal.reject(MDNException.parsing("getRequest #2"))
                    }
                case .failure(let error):
                    print("MDNKit > GET REQUEST Failure Response for \(getURI)")
                    seal.reject(MDNException.networkFailure(error))
                }
            }
        }
    }

    typealias PostBlock<T> = (_ doc: HTMLDocument) -> Result<T>
    func postRequest<T>(_ postURI: String, parameters: Parameters, headers: HTTPHeaders = [:], checkHTML: Bool = true, completionBlock: @escaping PostBlock<T>) -> Promise<T> {
        print("MDNKit > POST REQUEST \(postURI) \(headers)")

        return Promise { seal in
            guard let URL = forumURL else {
                seal.reject(MDNException.clientNotConfigured)
                return
            }

            var hdrs: HTTPHeaders = [
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            hdrs.merge(headers) { (_, new) in new }

            Alamofire.request(URL + postURI, method: .post, parameters: parameters, headers: hdrs).responseString { response in                print("MDNKit > POST REQUEST Response \(postURI)")

//                print("MDNKit > POST REQUEST Response \(response.request?.httpBody.)")
                //print(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue))
                //print(response.response?.allHeaderFields)
                //print(response.response?.statusCode)
                if var html = response.result.value {
                    print("MDNKit HTML > \(html)")

                    if !checkHTML && html.count == 0 {
                        html = "<OK>"
                    }

                    if let doc = try? HTML(html: html, encoding: .utf8) {

                        let result = completionBlock(doc)

                        switch result {
                        case .success(let result):
                            seal.fulfill(result)
                        case .error(let err):
                            seal.reject(err)
                        }

                    } else {
                        seal.reject(MDNException.parsing("postRequest #1"))
                    }

                } else {
                    seal.reject(MDNException.parsing("postRequest #2"))
                }
            }
        }
    }

    // MARK: API: Authentification
    func login(user: String, password: String) -> Promise<String> {

        let parameters: Parameters = [
            "pseudo": user,
            "password": password
        ]

        return MDN.client.postRequest(loginURI, parameters: parameters) { htmlDoc in
            let pattern = "<meta http-equiv=\"Refresh\" content=\"([0-9]); url="

            if let html = htmlDoc.toHTML {

                let regex = try! NSRegularExpression(pattern: pattern)
                let results = regex.matches(in: html,
                                            range: NSRange(html.startIndex..., in: html))

                if let range = results.first?.range(at: 1) {
                    if let value = Int(html.substring(with: Range(range, in: html)!)) {
                        switch value {
                        case 1:
                            return .success("MDN Client: Authenticated")
                        case 2:
                            return .error(MDNException.invalidCredentials)
                        default:
                            return .error(MDNException.parsing("login #1"))
                        }
                    } else {
                        return .error(MDNException.parsing("login #2"))
                    }
                } else {
                    return .error(MDNException.parsing("login #3"))
                }
            } else {
                return .error(MDNException.parsing("login #4"))
            }
        }
    }

    func logout() {

        let sharedCookieStorage = HTTPCookieStorage.shared
        if let cookies = sharedCookieStorage.cookies {
            for cookie in cookies {
                sharedCookieStorage.deleteCookie(cookie)
            }
        }
    }

    // MARK: API: Sections List

    // Categories       GET
    func getSections(of section: Section?) -> Promise<[Section]> {

        return MDN.client.getRequest(section?.url ?? "") { htmlDoc in

            var sectionLinksXPATH = "//a[@class='cCatTopic']"

            if section != nil {
                sectionLinksXPATH = "//tr[contains(@class, 'fondForum1Subcat')]//a[@class='cHeader']"
            }

            let links = htmlDoc.xpath(sectionLinksXPATH)

            var sections = [Section]()

            for link in links {
                if let sectionName = link.text, let sectionURL = link["href"] {
                    if sectionURL.range(of: "cat=prive") == nil {
                        let sec = Section(name: sectionName, url: sectionURL)
                        sections.append(sec)
                    }
                }
            }

            if links.count > 0 {
                return .success(sections)
            } else {
                return .error(MDNException.empty)
            }
        }
    }

    // MARK: API: Threads for Sections, MP & Starred
    //TODO Add Flag Filter and Pages for .section and .messages
    func getThreads(for section: Section,
                    filter: SectionFilter = ThreadType.section.defaultFilter()) -> Promise<ThreadsResults> {
        return MDN.client.getRequest(section.url(for: filter) ?? section.url) { htmlDoc in
            return MDNParser.parseThreads(htmlDoc: htmlDoc, listType: .section)
        }
    }

    func getThreads(for section: Section,
                    filter: SectionFilter = ThreadType.section.defaultFilter(),
                    forPage page: String) -> Promise<ThreadsResults> {

        return MDN.client.getRequest(section.url(for: filter, page: page) ?? section.url) { htmlDoc in
            return MDNParser.parseThreads(htmlDoc: htmlDoc, listType: .section, forPage: page)
        }
    }

    func getMessages() -> Promise<ThreadsResults> {
        return MDN.client.getRequest(messagesURI+ThreadType.message.defaultFilter().rawValue) { htmlDoc in
            return MDNParser.parseThreads(htmlDoc: htmlDoc, listType: .message)
        }
    }

    func getMessages(forPage page: String) -> Promise<ThreadsResults> {
        return MDN.client.getRequest(messagesURI.replacingOccurrences(of: "page=1",with: "page=\(page)")+ThreadType.message.defaultFilter().rawValue) { htmlDoc in
            return MDNParser.parseThreads(htmlDoc: htmlDoc, listType: .message, forPage: page)
        }
    }

    func getStarredThreads(filter: SectionFilter = ThreadType.starred.defaultFilter()) -> Promise<ThreadsResults> {
        print("Default filter \(ThreadType.starred.defaultFilter().rawValue)")
        return MDN.client.getRequest(favoritesURI+filter.rawValue) { htmlDoc in
            return MDNParser.parseStarredThreads(htmlDoc: htmlDoc)
        }
    }

    func removeStarred(withForm inputs: FormInputs) -> Promise<String> {
        var parameters = Parameters()

        inputs.forEach { parameters[$0] = $1 }
        parameters["action_reaction"] = "message_forum_delflags"
        parameters["valueforum0"] = "hardwarefr"

        return MDN.client.postRequest(removeStarredURI, parameters: parameters, checkHTML: false) { htmlDoc in
            if let messageNode = htmlDoc.at_xpath("//div[@class='hop'][1]") {

                if messageNode.at_xpath(".//a") != nil || messageNode.at_xpath(".//input") != nil {
                    return .error(MDNException.parsing(messageNode.text!.trimmingCharacters(in: .whitespacesAndNewlines)))
                } else {
                    return .success("")
                }
            } else {
                return .success("")
            }

        }
    }

    func addStarred(withURL url: String) -> Promise<String> {

        return MDN.client.getRequest(url) { htmlDoc in

            if let messageNode = htmlDoc.at_xpath("//div[@class='hop'][1]") {

                if messageNode.at_xpath(".//a") != nil || messageNode.at_xpath(".//input") != nil {
                    return .error(MDNException.parsing(messageNode.text!.trimmingCharacters(in: .whitespacesAndNewlines)))
                } else {
                    return .success("OK #1")
                }
            } else {
                return .success("OK #2")
            }

        }
    }
    // Threads|MPs      GET (pagination) | CREATE
    //  func getPosts(thread: Thread, page: Int) -> Promise<[Post]> {
    //    return Promise { fulfill, reject in
    //
    //    }
    //
    //  }

    // MARK: API: Posts
    // Thread->Posts    GET (pagination) | SEARCH
    /* FIXME
     func getPosts(thread: Thread, type: .lastRead | .first | .lastPage | .last) -> Promise<[Post]> {

     return MDN.client.getRequest(thread.URL(type)) { htmlDoc in

     */

    func getPosts(at url: String) -> Promise<PostsResults> {

        return MDN.client.getRequest(url) { htmlDoc in
            // Handle empty page / wrong page number

            var posts = [PostID: Post]()
            let thread = Thread(url: url)

            // Thread pages parsing
            if let pageNode = htmlDoc.at_xpath("//tr[@class='cBackHeader fondForum2PagesHaut']//div[@class='left']//*[last()]") {
                thread.lastPage = pageNode.text
            } else {
                thread.lastPage = "1"
            }
            if let titleNode = htmlDoc.at_xpath("//tr[@class='cBackHeader fondForum2Title']//h3") {
                thread.name = titleNode.text
            } else {
                return .error(MDNException.parsing("No Title"))
            }

            if let formNode = htmlDoc.at_xpath("//form[@id='repondre_form']") {
                thread.newAnswerURL = formNode["action"]!
            } else {
                thread.newAnswerURL = nil
            }

            // fastAnswer
            if let fastformNode = htmlDoc.at_xpath("//form[@name='hop']") {
                var inputs = [String: String]()

                let inputNodes = fastformNode.xpath(".//input")
                for node in inputNodes {

                    if let name = node["name"], let value = node["value"] {

                        if name == "MsgIcon" {
                            if node["checked"] == "checked" {
                                inputs[name] = value
                            }
                        } else if node["type"] == "checkbox" {
                            if node["checked"] == "checked" {
                                inputs[name] = "1"
                            } else {
                                inputs[name] = "0"
                            }
                        } else {
                            inputs[name] = value
                        }
                    }
                }

                let selectNodes = fastformNode.xpath(".//select")
                for node in selectNodes {
                    if let name = node["name"] {
                        // If one option have the selected tag we use this one
                        // or we use the First one.
                        if let svalue = node.at_xpath(".//option[@selected='selected']")?["value"] {
                            print("selected \(name) \(svalue)")
                            inputs[name] = svalue
                        } else {
                            print("Pas selected \(name) ")
                            inputs[name] = node.at_xpath(".//option[1]")?["value"]
                        }
                    }
                }

                if let content = fastformNode.at_xpath(".//textarea[@id='content_form']")?.content {
                    inputs["content_form"] = content.replacingOccurrences(of: "\r\n", with: "\n")
                }
                thread.fastAnswerInputs = inputs
            }
            //.fastAnswer

            //.Thread pages parsing

            // Images Cache
            //let fileManager = FileManager.default
            //let urls = fileManager.urls(for: .itemReplacementDirectory, in: .userDomainMask)
            let tmpDirectoryURL = FileManager.default.temporaryDirectory
            let avatarsDirectoryURL = tmpDirectoryURL.appendingPathComponent("MDN-Avatars", isDirectory: true)

            let avatarQueue = OperationQueue()
            avatarQueue.qualityOfService = .userInitiated
            avatarQueue.maxConcurrentOperationCount = OperationQueue.defaultMaxConcurrentOperationCount

            // Posts
            let postNodes = htmlDoc.xpath("//table[@class='messagetable']")

            let dateStart = Date()
            for postNode in postNodes {

                if let rawContent = postNode.innerHTML {
                    let tmp = Post(rawNode: rawContent)

                    tmp.author = (postNode.at_xpath(".//b[@class='s2']")?.content)!
                    if tmp.author == "Publicité" { continue }
                    if tmp.author == "Modération" { tmp.fromModeration = true }
                    //TODO Custom style for Moderation posts / Deleted posts
                    tmp.id = (postNode.at_xpath(".//td[contains(@class, 'messCase1')]//a[1]")?["name"]?.replacingOccurrences(of: "t", with: ""))!

                    // Avatar
                    if let avatarNode = postNode.at_xpath(".//div[@class='avatar_center']//img") {
                        //tmp.authorAvatarURL = avatarNode["src"]!

                        //print("User: \(tmp.author) \t\t \(tmp.author.get_sha256_String())")

                        avatarQueue.addOperation {
                            let avatarURL = avatarsDirectoryURL.appendingPathComponent(tmp.author.get_sha256_String(), isDirectory: false)

                            do {
                                _ = try avatarURL.checkResourceIsReachable()

                                tmp.authorAvatarURL = avatarURL.absoluteString//"\(tmp.author.get_sha256_String())"
                            } catch {
                                //print("Avatar KO... Downloading for \(tmp.author)")
                                do {
                                    let avatarData = try Data(contentsOf: URL(string: avatarNode["src"]!)!)

                                    do {
                                        try avatarData.write(to: avatarURL)
                                        //print("Avatar KO.. Downloading OK... Write To file OK")
                                        tmp.authorAvatarURL = avatarURL.absoluteString//"\(tmp.author.get_sha256_String())"
                                    } catch {
                                        //print("Avatar KO.. Downloading OK... Write To file KO")
                                    }
                                } catch {
                                    //print("Avatar KO.. Downloading KO")
                                }

                            }
                        } // /Operation
                    }



                    tmp.rawContent = (postNode.at_xpath(".//div[contains(@id, 'para')]")?.toHTML)!

                    // Post icon
                    if let iconNode = postNode.at_xpath(".//td[@class='messCase1']//div[@class='right']//img[contains(@src, '/icones/message/')]") {
                        if let iconURL = iconNode["src"] {
                            tmp.iconURL = iconURL
                        }
                    }

                    // URL to answer with quote to specific post
                    if let quoteURLNode = postNode.at_xpath(".//img[@alt='answer']/..") {
                        if let quoteURLClass = quoteURLNode["class"] {
                            tmp.quoteURL = quoteURLClass
                        }
                    }

                    // URL to edit own post
                    if let editURLNode = postNode.at_xpath(".//img[@alt='edit']/..") {
                        if let editURLClass = editURLNode["class"] {
                            tmp.editURL = editURLClass
                        }
                    }

                    // String used to add/remove multi-quote cookies
                    if let quoteJSNode = postNode.at_xpath(".//a[contains(@onclick, \"quoter('hardwarefr'\")]") {
                        tmp.multiQuoteString = quoteJSNode["onclick"]!
                    }

                    // URL to alert on specific post
                    if let alertNode = postNode.at_xpath(".//a[contains(@href, '/user/modo.php')]") {
                        tmp.alertURL = alertNode["href"]!
                    }

                    // URL to send MP to post author
                    if let privateNode = postNode.at_xpath(".//img[@alt='MP']/..") {
                        tmp.privateMessageURL = privateNode["href"]!
                    }

                    // URL to put star on speccific post
                    if let starNode = postNode.at_xpath(".//a[contains(@href, '/user/addflag.php')]") {
                        tmp.starringPostURL = starNode["href"]!
                    }

                    // User profile URL
                    if let profileNode = postNode.at_xpath(".//img[@alt='profil']/..") {
                        tmp.userProfileURL = profileNode["href"]!
                    }

                    // EditedAt
                    if let editedNode = postNode.at_xpath(".//div[@class='edited']") {
                        if let quotedNode = editedNode.at_xpath(".//a[1]") {

                            let quotePattern = ".*([0-9]+).*"

                            if let matches = quotedNode.content?.regexMatch(forPattern: quotePattern), matches.count == 1 {
                                tmp.quoted = "\(matches[0])"
                                tmp.quotedURL = quotedNode["href"]
                            }

                            editedNode.removeChild(quotedNode)
                        }
                        tmp.editedAt = editedNode.innerHTML?.replacingOccurrences(of: "<br>Message ", with: "").trimmingCharacters(in: .whitespacesAndNewlines)
                    }

                    // PostedAt
                    if let dateNode = postNode.at_xpath(".//div[@class='toolbar']") {
                        let datePattern = ".*([0-9]{2})-([0-9]{2})-([0-9]{4}).*([0-9]{2}):([0-9]{2}):([0-9]{2}).*"

                        if let matches = dateNode.content?.regexMatch(forPattern: datePattern), matches.count == 6 {
                            tmp.date = "\(matches[0])/\(matches[1])/\(matches[2]) \(matches[3]):\(matches[4]):\(matches[5])"
                        }
                    }

                    posts[tmp.id] = tmp

                }

            }

            avatarQueue.waitUntilAllOperationsAreFinished()

            let dateEnd = Date()
            print("Parsings Posts = \(dateEnd.timeIntervalSince(dateStart))")
            if posts.count > 0 {
              return .success(PostsResults(thread: thread, posts: posts))
            } else {
              return .error(MDNException.empty)
            }

        }
    }

    // MARK: API: Forms
    // Thread->Post     CREATE | UPDATE | DELETE
    func getSmilies(for keyword: String) -> Promise<[Smiley]> {

        var encodedKeywords = "+\(keyword.split(separator: " ").joined(separator: " +"))"
        encodedKeywords = encodedKeywords.addingPercentEncoding(withAllowedCharacters: .alphanumerics)!

        return MDN.client.getRequest(wikiSmileyURI+encodedKeywords) { htmlDoc in

            var smileys = [Smiley]()
            let smileyNodes = htmlDoc.xpath("//img")
            for node in smileyNodes {
                if let code = node["alt"], let url = node["src"] {
                    smileys.append(Smiley(code: code, url: url))
                }
            }

            if smileys.count > 0 {
                return .success(smileys)
            } else {
                return .error(MDNException.empty)
            }

        }
    }


    func getAlertFormInputs(with url: String) -> Promise<AlertFormResults> {
        return MDN.client.getRequest(url) { htmlDoc in

            if let messageNode = htmlDoc.at_xpath("//div[@class='hop'][1]") {
                if messageNode.at_xpath(".//a") != nil || messageNode.at_xpath(".//input") != nil {
                    return .error(MDNException.alreadyAlerted)
                } else {
                    return .error(MDNException.parsing("getAlertFormInputs #1"))
                }

            } else if let formNode = htmlDoc.at_xpath("//form[contains(@action, 'modo.php')]") {
                var inputs = [String: String]()

                let inputNodes = formNode.xpath(".//input")
                for node in inputNodes {
                    if let name = node["name"], let value = node["value"] {
                        inputs[name] = value
                    }
                }

                if inputs.count != 0, let submitURL = formNode["action"] {
                    let results = AlertFormResults(inputs: inputs, submitURL: "/user/\(submitURL)")
                    return .success(results)
                } else {
                    return .error(MDNException.parsing("getAlertFormInputs #2"))
                }


            } else {
                return .error(MDNException.parsing("getAlertFormInputs #3"))
            }
        }
    }
    func getFullFormInputs(with url: String) -> Promise<PostFormResults> {
        return MDN.client.getRequest(url) { htmlDoc in

            if let formNode = htmlDoc.at_xpath("//form[@name='hop']") {
                var inputs = [String: String]()

                let inputNodes = formNode.xpath(".//input")
                for node in inputNodes {

                    if let name = node["name"], let value = node["value"] {

                        if name == "MsgIcon" {
                            if node["checked"] == "checked" {
                                inputs[name] = value
                            }
                        } else if node["type"] == "checkbox" {
                            if node["checked"] == "checked" {
                                inputs[name] = "1"
                            } else {
                                inputs[name] = "0"
                            }
                        } else {
                            inputs[name] = value
                        }
                    }
                }

                let selectNodes = formNode.xpath(".//select")
                for node in selectNodes {
                    if let name = node["name"] {
                        // If one option have the selected tag we use This one
                        // or we use the First one.
                        if let svalue = node.at_xpath(".//option[@selected='selected']")?["value"] {
                            print("selected \(name) \(svalue)")
                            inputs[name] = svalue
                        } else {
                            print("Pas selected \(name) ")
                            inputs[name] = node.at_xpath(".//option[1]")?["value"]
                        }
                    }
                }

                if let content = formNode.at_xpath(".//textarea[@id='content_form']")?.content {
                    inputs["content_form"] = content.replacingOccurrences(of: "\r\n", with: "\n")
                }

                //TODO MP/"Recipient name" (not an input)

                // base Smileys
                var baseSmileys = [Smiley]()
                let baseSmileyNodes = htmlDoc.xpath("//div[@class='smiley']//img")
                for node in baseSmileyNodes {
                    if let code = node["alt"], let url = node["src"] {
                        baseSmileys.append(Smiley(code: code, url: url))
                    }
                }

                // starred Smileys
                var starredSmileys = [Smiley]()
                let starredSmileyNodes = htmlDoc.xpath("//div[@id='dynamic_smilies']//img")
                for node in starredSmileyNodes {
                    if let code = node["alt"], let url = node["src"] {
                        starredSmileys.append(Smiley(code: code, url: url))
                    }
                }

                let results = PostFormResults(inputs: inputs, baseSmileys: baseSmileys, starredSmileys: starredSmileys)
                return .success(results)
            } else {
                return .error(MDNException.parsing("getFullFormInputs #1"))
            }
        }
    }

    func sendAlertForm(with postURL: String, inputs: [String:String]) -> Promise<MDNSendPostResult> {
        var parameters = Parameters()
        for (key, value) in inputs {
            if key == "raison" {
                parameters[key] = value.replacingOccurrences(of: "\n", with: "\r\n")
            } else {
                parameters[key] = value
            }
        }

        return MDN.client.postRequest(postURL, parameters: parameters) { htmlDoc in

            print("====")
            print(htmlDoc.toHTML!)

            if let messageNode = htmlDoc.at_xpath("//div[@class='hop']") {

                print("====")
                print(messageNode.toHTML!)

                if messageNode.at_xpath(".//a") != nil || messageNode.at_xpath(".//input") != nil {
                    return .error(MDNException.native(messageNode.text!.trimmingCharacters(in: .whitespacesAndNewlines)))
                } else {
                    return .success(MDNSendPostResult(message: messageNode.innerHTML!.trimmingCharacters(in: .whitespacesAndNewlines),
                                                      anchor: ""))
                }
            } else {
                return .error(MDNException.parsing("sendAlertForm #1"))
            }
        }
    }

    //FIXME
    //func sendForm(withInputs inputs: [String:String], url: String, deleteMode: Bool = false) -> Promise<String> {

    func sendPostForm(for mode: FormMode, with inputs: [String:String]) -> Promise<MDNSendPostResult> {
        //print("postForm with input \(inputs) = \(deleteMode)")

        var parameters = Parameters()
        for (key, value) in inputs {
            if key == "allowvisitor" || key == "have_sondage" || key == "sticky" || key == "sticky_everywhere" {
                if value == "1" {
                    parameters[key] = 1
                }
            } else if key == "delete" && mode == .deletion {
                parameters[key] = 1
            } else if key == "content_form" {
                parameters[key] = value.replacingOccurrences(of: "\n", with: "\r\n")
            } else {
                parameters[key] = value
            }
        }

        //FIXME Get form action from Source
        var postURL = ""
        if let numr = inputs["numreponse"] {
            //print("numrep c= \(numr)")
            if numr.count > 0 {
                postURL = "/bdd.php"
            } else {
                postURL = "/bddpost.php"
            }
        } else {
            postURL = "/bddpost.php"
        }

        return MDN.client.postRequest(postURL, parameters: parameters) { htmlDoc in
            //print("====")
            //print(htmlDoc.toHTML!)
            if let messageNode = htmlDoc.at_xpath("//div[@class='hop'][1]") {
                //print("====")
                //print(messageNode.toHTML!)

                if messageNode.at_xpath(".//a") != nil || messageNode.at_xpath(".//input") != nil {
                    //print("==== \(postURL)")
                    //print(messageNode.content!)
                    return .error(MDNException.native(messageNode.text!.trimmingCharacters(in: .whitespacesAndNewlines)))
                } else {
                    var newAnchor: String?

                    let html = htmlDoc.toHTML!
                    let pattern = "<meta http-equiv=\"Refresh\" content=\"[^#]+#t([^\"]*)\">"
                    let regex = try! NSRegularExpression(pattern: pattern, options: [])

                    let results = regex.matches(in: html,
                                                options: [],
                                                range: NSRange(location: 0,
                                                               length: html.count))

                    if let result = results.first {
                        newAnchor = (html as NSString).substring(with: result.range(at: 1))
                    }

                    return .success(MDNSendPostResult(message: messageNode.innerHTML!.trimmingCharacters(in: .whitespacesAndNewlines),
                                                      anchor: newAnchor))
                }
            } else {
                return .error(MDNException.parsing("sendPostForm #1"))
            }

        }
    }

    func readCookie(name: String) -> String {
        let cookiesStorage = HTTPCookieStorage.shared
        if let cookies = cookiesStorage.cookies {
            for cookie in cookies {
                if cookie.name == name {
                    if Date().timeIntervalSince(cookie.expiresDate!) <= 0 {
                        return cookie.value
                    }
                }
            }
        }

        return ""
    }

    func deleteCookie(name: String) {
        let cookiesStorage = HTTPCookieStorage.shared
        if let cookies = cookiesStorage.cookies {
            for cookie in cookies {
                if cookie.name == name {
                    cookiesStorage.deleteCookie(cookie)
                }
            }
        }
    }

    func writeCookie(name: String, with value: String) {
        var dict = [HTTPCookiePropertyKey: Any]()
        dict[.name] = name
        dict[.value] = value
        dict[.domain] = ".hardware.fr"
        dict[.expires] = Date().addingTimeInterval(60*60)
        dict[.path] = "/"

        if let cookie = HTTPCookie(properties: dict) {
            let cookiesStorage = HTTPCookieStorage.shared
            cookiesStorage.setCookie(cookie)
        }
    }

}
