//
//  MDNTemplateHelper.swift
//  Infinity
//
//  Created by FLK on 18/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

// swiftlint:disable type_body_length function_body_length file_length
class Template {
  static let css = TemplateCSS()
  static let html = TemplateHTML()
}

internal class TemplateHTML {

  func thread(_ posts: [Post]) -> String {
    return """
    <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml' xml:lang='fr' lang='fr'>
    <head>
    <meta name='viewport' content='initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no' />
    <style type='text/css'>
    \(Template.css.thread())
    </style>
    </head>
    <body onload="startup()">
        <div class='nosig'>
          <div id='mdn_top'></div>

          \(posts.toHTML())
          <div id='mdn_bottom'></div>
        </div>
        <script type="text/javascript">
          function startup() {
            document.addEventListener('touchstart', touchStart, false);
          }

          function sharedMenuFor(obj, evt) {
            evt.preventDefault();
    
            var rect = obj.getBoundingClientRect();
            window.webkit.messageHandlers.notification.postMessage({"type": "menu",
                                                                    "id": obj.parentNode.id.replace('mdn_post', ''),
                                                                    "yPos": rect.top,
                                                                    "clientX": evt.clientX });
            return false;
          }

          function touchStart(evt) {
            window.webkit.messageHandlers.notification.postMessage({"type": "touch" });
          }

          function swap_spoiler_states(obj) {
            var div=obj.getElementsByTagName('div');
            if(div[0]) {
              if(div[0].style.visibility == "visible") { div[0].style.visibility = 'hidden'; }
              else if(div[0].style.visibility == "hidden" || !div[0].style.visibility ) { div[0].style.visibility = 'visible'; }
            }
          }
        </script>
    </body>
    </html>
    """
  }

  func post(_ post: Post) -> String {
    var postEditedAt = ""
    if let _ = post.editedAt?.count {
      postEditedAt = "<p class=\"editedhfrlink\">\(post.editedAt!)</p>"
    }
    
    var quotedDiv = ""
    if let quoted = post.quoted, let quotedURL = post.quotedURL {
      quotedDiv = "<a class=\"quotedhfrlink\" href=\"\(quotedURL)\">\(quoted)</a>"
    }

    var iconDiv = ""
    if let icon = post.iconURL {
      iconDiv = "<img src='\(icon)' />"
    }

    var moderation = ""
    if post.fromModeration {
      moderation = "mdn_moderation"
    }
    
//<a class="awesome small"> ▾ </a>
    return """
    <div class="message \(moderation)" id="mdn_post\(post.id)">
      <div class="header" onClick="return sharedMenuFor(this, event);">

        <div class="left2" style="background-image:url('\(post.authorAvatarURL ?? "none")');"></div>
        <div class="right">\(iconDiv)</div>
        <div class="left">\(post.author)</div>
        <div class="leftdate">\(post.date)</div>
        <div class="clear"></div>

      </div>
      <div class="content">
        <div class="right">
        \(post.rawContent)
        \(quotedDiv)
        \(postEditedAt)
        </div>
        <div class="clear"></div>

      </div>

      <div class="clear"></div>
    </div>
    """.replacingOccurrences(of: "href=\"/", with: "href=\"local:///")
  }

}
class TemplateCSS {
  //TODO: Split Colors & UI
  func thread() -> String {
    return """
    * {
      padding:0 !important;
      margin:0 !important;
      padding:0 !important;
      font-family: "San Francisco", "Helvetica Neue", "Lucida Grande" !important;



      /*-webkit-user-select: none;

      -webkit-touch-callout: none;

      -webkit-tap-highlight-color:rgba(0,0,0,0);*/
      -webkit-text-size-adjust: none;
    }

    .maxmessage {
      padding-bottom:54px !important;
    }

    .clear {
      clear:both;
    }

    .hfrplusimg {
      margin:2px 0 !important;
    }

    img {
      max-width: 100% !important;
    }

    .last_read {
      border-right: 10px solid rgb(239, 239, 244) !important;
      background: white !important
    }

    span.signature {
      display: block;
      border-top: 1px dashed lightgray;
      padding-top: 10px !important;
    }

    span.signature ~ * {
      display:none;
    }

    .message .content .right span.signature {
      font-size: 0.85em !important;
    }

    span.signature > br:first-child {
      display: none;
    }

    .nosig span.signature, div.edited {
      display:none;
    }

    table {
      border: 1px solid silver;
      padding: 4px !important;
      width:100%;
      max-width:100% !important;
      margin-bottom:5px !important;
    }

    table p {
      max-width:100% !important;
      overflow-x:hidden;
    }

    table p img {
      max-width:100% !important;
    }

    table .s1 a {
      font-weight:bold !important;
      text-decoration:underline !important;
      color:black !important;
    }


    div.masque {
      visibility:hidden;
    }

    .message {
      overflow:hidden;
    }


    
    .anchorlink {
      display:block;
    }

    .message .header {
      -webkit-user-select: none;
      -webkit-touch-callout:none;
      -webkit-tap-highlight-color:rgba(0,0,0,0);
      height:34px;
      padding:0 10px !important;
      margin-top:5px !important;
      border-top:1px solid #717d85;
      border-bottom:1px solid #989ea4;
    }

    .message .header .left2 {
      text-align:left;
      float:left;
      width:56px;
      height:30px;
      margin-top:1px !important;

      border:1px solid #717d85;

      background-size: cover;
      background-repeat: no-repeat;
      background-position:center;
    }
    .message .header .left2.noavatar {
    background-image:url('avatar_male_gray_on_light_48x48.png');
    }


    .message .header .left {
    margin-left:10px !important;
    /*margin-top:2px !important;*/

    font-size:13px !important;
    line-height:20px;
    color:white;
    float:left;
    font-weight:bolder;
    width:60%;
    text-shadow: #555 0px 1px 1px;
    height:19px;

    }

    .message .header .leftdate {
    margin-left:10px !important;

    font-size:9px !important;
    line-height:14px;
    color:white;
    float:left;
    font-weight:bold;
    width:60%;
    text-shadow: #555 0px 1px 1px;
    height:14px;

    }

    .message .header .right {
      float: right;
      height: 34px;
      position:relative;
    }

    .message .header .right img {
      position: absolute;
      top: 50%;

      right: 50%;
      transform: translate(0%, -50%);
    }
    .message .content {

      padding:0 !important;
      background:white;
    }


    .message .content .right {
    text-align:left;
    float:right;
    width:100%;
    overflow:hidden;
    font-size:13px !important;
    }
    .message .content .right * {
    font-size:1em !important;
    }

    .message .content .right a {
    color:#2470d8;

    }

    .message .content .right ol, ul
    {
    margin:1em 0px !important;
    padding-left:2em !important
    }

    .message .content .right table.code td {
    font-size:0.8em !important;
    }

    table.code, table.fixed, table.quote, table.citation, table.oldcitation {
    table-layout:fixed;
    border: none;
    border-left: 3px solid  silver;
    }

    hr {
    visibility:hidden;
    height:12px;
    }

    hr:last-child {
    display:none;
    }

    table.code p, table.fixed p {
    overflow:scroll;
    -webkit-overflow-scrolling: touch;
    white-space:nowrap;
    }

    table.code *, table.fixed * {
    font-family: monospace !important;
    }

    #actualiserbtn {
    color:#2470d8;
    line-height:44px;
    font-size:15px !important;
    font-weight:bold;
    border-top:1px solid #e0e0e0;
    border-bottom:1px solid #e0e0e0;
    text-align:center;
    width:100%;
    background:white;
    -webkit-user-select: none;
    -webkit-touch-callout:none;
    -webkit-tap-highlight-color:rgba(0,0,0,0);

    }

    #actualiserbtn.loading {
    background-image: url(loadinfo.gif);
    background-repeat: no-repeat;
    background-position: 99px 14px;
    background-size: 15px;
    }

    #toolbarpage * {
    -webkit-user-select: none;
    -webkit-touch-callout:none;
    -webkit-tap-highlight-color:rgba(0,0,0,0);
    }

    #toolbarpage {
    -webkit-user-select: none;
    -webkit-touch-callout:none;
    -webkit-tap-highlight-color:rgba(0,0,0,0);
    color:#FFF;
    line-height:43px;
    font-size:15px !important;
    font-weight:bold;
    border-top:1px solid #000;
    text-align:center;
    position:relative;
    background-image: url(toolbar3.png);
    height:43px;
    width:100%;
    text-shadow: rgba(0, 0, 0, 0.6) 0px -1px 1px;

    }
    #toolbarpage a {
    -webkit-user-select: none;
    -webkit-touch-callout:none;
    -webkit-tap-highlight-color:rgba(0,0,0,0);

    color:#FFF;
    text-decoration:none;
    }
    /*38x31*/
    #toolbarpage .button {

    position:absolute;
    width:39px;
    height:30px;
    top:7px;
    }

    #toolbarpage .button a {
    width:39px;
    height:30px;
    display:block;
    text-indent: -100px;
    overflow: hidden;
    }

    #toolbarpage .button.begin {
    left:6px;
    background-image:url(buttonbeginoff.png);
    }
    #toolbarpage .button.begin.active {
    background-image:url(buttonbeginon.png);
    }
    #toolbarpage .button.begin.hover {
    background-image:url(buttonbeginhover.png);
    }
    #toolbarpage .button.end {
    right:6px;
    background-image:url(buttonendoff.png);
    }
    #toolbarpage .button.end.active {
    background-image:url(buttonendon.png);
    }
    #toolbarpage .button.end.hover {
    background-image:url(buttonendhover.png);
    }

    #toolbarpage .button2 {
    position:absolute;
    width:32px;
    height:30px;
    top:7px;
    }

    #toolbarpage .button2 a {
    width:32px;
    height:30px;
    display:block;
    text-indent: -100px;
    overflow: hidden;
    }

    #toolbarpage .button2.begin {
    left:55px;
    background-image:url(buttonpreviousoff.png);
    }
    #toolbarpage .button2.begin.active {
    background-image:url(buttonpreviouson.png);
    }
    #toolbarpage .button2.begin.hover {
    background-image:url(buttonprevioushover.png);
    }
    #toolbarpage .button2.end {
    right:55px;
    background-image:url(buttonnextoff.png);
    }
    #toolbarpage .button2.end.active {
    background-image:url(buttonnexton.png);
    }
    #toolbarpage .button2.end.hover {
    background-image:url(buttonnexthover.png);
    }

    .u {
    text-decoration:underline;
    }

    .smileycustom {
    max-height:30px;
    }


    .message .content .right a.quotedhfrlink {
    background-image:url('08-chat@2x.png') !important;
    background-size: 24px 22px;
    background-position:top right !important;
    background-repeat:no-repeat !important;
    color:white !important;
    float:right;
    display:block !important;
    height:22px !important;
    width:24px !important;
    padding-left:1px !important;
    line-height:16px !important;
    font-size:0.9em !important;
    font-style:normal;
    text-align:center;
    text-decoration:none;
    font-weight:bold;
    -webkit-user-select: none;
    -webkit-touch-callout:none;
    -webkit-tap-highlight-color:rgba(0,0,0,0);
    }

    .message .content .right p.editedhfrlink {
    font-size:0.7em !important;
    font-style:italic;
    text-align:right;
    clear:both;
    -webkit-user-select: none;
    -webkit-touch-callout:none;
    -webkit-tap-highlight-color:rgba(0,0,0,0);
    }

    .message .content {
    /*    position:relative;*/
    }
    .message .content .right {
    /*    padding-top: 44px !important; */
    }
    .message .content .right>a.cLink {
    display: block;
    line-height: 44px;
    padding: 0 0px 0 10px !important;
    text-decoration: none;
    color: black;
    border-bottom: 1px solid #efeff4;
    background: url(unselectedrow.png) no-repeat scroll right center white;
    background-origin: content-box;
    }

    div[id^=para] {
    padding: 10px !important;
    word-wrap: break-word;
    visibility: collapse;
    }
    
    div[id^=para] > * {
    visibility: visible;
    }
    
    .message .content .right .quotedhfrlink, .message .content .right .editedhfrlink {
    margin-right: 10px !important;
    margin-bottom: 10px !important;
    }

    a#searchintra_nextbutton {
    position: fixed;
    bottom: 0;
    right: 0;
    width: 100%;
    display: block;
    line-height: 44px;
    padding: 0 0px 0 10px !important;
    text-decoration: none;
    color: white;
    font-weight: normal;
    text-align: center;
    font-size: 0.9em;

    border-top:1px solid #717d85;


    background:-webkit-gradient(
    linear,
    left bottom,
    left top,
    color-stop(0, rgb(164,178,189)),
    color-stop(1, rgb(140,156,169))
    );
    }



    /* iOS7 */



    .searchintra {
    padding-bottom:44px !important;
    }

    a#searchintra_nextbutton {
    color: rgba(109, 109, 114, 1);
    background: rgba(239, 239, 244, 0.9);
    border-top: 1px solid rgba(109, 109, 114, 0.1);
    }

    .message .content .right>a.cLink {
    padding: 0 5px 0 10px !important;
    }

    .message .header {
    margin-top:0 !important;
    padding-top: 10px !important;
    padding-bottom: 10px !important;
    background:rgb(239, 239, 244);
    color: rgba(109, 109, 114, 1);
    border:none;
    }

    .message .header .left, .message .header .leftdate {
    color: rgba(109, 109, 114, 1);
    text-shadow:none;
    }

    .message .header .leftdate {
    font-weight:normal
    }

    .message.del {
      opacity:0.5;
    }
    .message.mdn_moderation .content {
      background-color: #ffdce0;
    }
    .message.mdn_moderation .header {
      background-color: #ffccce;
    }
    .message.mdn_moderation .header .left2 {
      display:none;
    }
    .message.mdn_moderation .header .left, .message.mdn_moderation .header .leftdate {
      margin-left:0px !important;
      color: #062016 !important
    }
    
    .message.mdn_egoquote .container .citation {
      background-color: rgba(246, 225, 95, 0.44);
      border-left: 3px solid rgb(246, 225, 95);
    }
    
    #toolbarpage {
    background:#efeff4;
    text-shadow:none;
    border:none;
    padding-top:11px !important;
    }

    #toolbarpage a {
    color:black;
    }

    #toolbarpage .button {
    width:19px;
    height:15px;
    top:24px;
    background-size:100% 100%;
    }

    #toolbarpage .button2 {
    width:12px;
    height:15px;
    top:24px;
    background-size:100% 100%;
    }


    #toolbarpage .button a {
    width:17px;
    height:15px;
    display:block;
    text-indent: -100px;
    overflow: hidden;
    }

    #toolbarpage .button2 a {
    width:12px;
    height:15px;
    display:block;
    text-indent: -100px;
    overflow: hidden;
    }

    #toolbarpage .button.begin {
    left:16px;
    background-image:url(ToolBarFastRewind_off.png);
    }
    #toolbarpage .button.begin.active {
    background-image:url(ToolBarFastRewind_on.png);
    }
    #toolbarpage .button.begin.hover {
    opacity:0.4;
    }
    #toolbarpage .button.end {
    right:16px;
    background-image:url(ToolBarFastForward_off.png);
    }
    #toolbarpage .button.end.active {
    background-image:url(ToolBarFastForward_on.png);
    }
    #toolbarpage .button.end.hover {
    opacity:0.4;
    }

    #toolbarpage .button2.begin {
    left:54px;
    background-image:url(ToolBarPrevious_off.png);
    }
    #toolbarpage .button2.begin.active {
    background-image:url(ToolBarPrevious_on.png);
    }
    #toolbarpage .button2.begin.hover {
    opacity:0.4;
    }
    #toolbarpage .button2.end {
    right:54px;
    background-image:url(ToolBarPlay_off.png);
    }
    #toolbarpage .button2.end.active {
    background-image:url(ToolBarPlay_on.png);
    }
    #toolbarpage .button2.end.hover {
    opacity:0.4;
    }

    /* -- iOS7 */

    /* iPhone 3.4 landscape */
    @media only screen and (orientation:landscape) {
    #actualiserbtn.loading {
    background-position: 180px 14px;
    }

    }

    /* iPhone 5 landscape */
    @media only screen and (orientation:landscape) and (device-height: 568px) {
    #actualiserbtn.loading {
    background-position: 225px 14px;
    }

    }

    /* iPhone 6 portrait */
    @media only screen and (orientation:portrait) and (device-width: 375px) {
    #actualiserbtn.loading {
    background-position: 130px 14px;
    }

    }

    /* iPhone 6 landscape */
    @media only screen and (orientation:landscape) and (device-width: 375px) {
    #actualiserbtn.loading {
    background-position: 275px 14px;
    }

    }

    /* iPhone 6+ portrait */
    @media only screen and (orientation:portrait) and (device-width: 414px) {
    #actualiserbtn.loading {
    background-position: 145px 14px;
    }

    }

    /* iPhone 6+ landscape */
    @media only screen and (orientation:landscape) and (device-width: 414px) {
    #actualiserbtn.loading {
    background-position: 275px 14px;
    }

    }

    /* iPad Portrait */
    @media only screen and (device-width: 768px) and (min-width: 767px) {
    #actualiserbtn.loading {
    background-position: 325px 14px;
    }

    .message .content .right * {
    font-size:1em !important;
    }
    }

    /* iPad Landscape */
    @media only screen and (device-width: 768px) and (min-width: 700px) and (max-width: 750px) {
    #actualiserbtn.loading {
    background-position: 295px 14px;
    }

    .message .content .right * {
    font-size:1em !important;
    }

    }


    /* terminator */
    .message.hfrbl {
    height:0px !important;
    overflow:hidden !important;
    }
"""
  }
}
