//
//  MDNLinkHelper.swift
//  Infinity
//
//  Created by flk on 17/10/2017.
//  Copyright © 2017 flk. All rights reserved.
//
import Foundation

enum MDNLinkType {
  case thread
  case profile
  case section
  case unknown
  case external
  case error
}

struct LinkHelperResult {
  var type: MDNLinkType
  var url: String
}

class Link {
  static let helper = Link()
  
  func type(of sourceURL: URL?) -> LinkHelperResult {
    print("Type of \(sourceURL)")
    print(sourceURL?.absoluteString)
    if let scheme = sourceURL?.scheme, let finalURL = sourceURL?.absoluteString {
      if scheme != "local" {
        return LinkHelperResult(type: .external, url: finalURL)
      } else {
        let relativeURL = finalURL.replacingOccurrences(of: "\(scheme)://", with: "")
        if relativeURL.firstMatch(forPattern: "^(\\/[a-z]+\\/[a-z]+\\/[^_]+_[0-9]+_[0-9]+.+)$") != nil {
          return LinkHelperResult(type: .thread, url: relativeURL)
        } else if relativeURL.firstMatch(forPattern: "^(\\/forum2\\.php\\?.*page=[0-9]+.*)$") != nil {
          return LinkHelperResult(type: .thread, url: relativeURL)
        } else {
          return LinkHelperResult(type: .unknown, url: finalURL)
        }
      }
    } else {
      return LinkHelperResult(type: .error, url: "<error>")
    }
    
    
    
  }
}
