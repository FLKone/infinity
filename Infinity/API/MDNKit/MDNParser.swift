//
//  MDNParser.swift
//  Infinity
//
//  Created by FLK on 29/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
import Kanna
import PromiseKit

class MDNParser {

  static func authNeeded(for listType: ThreadType) -> String? {
    switch listType {
    case .message:
      return "//body[@id='unique__topics_listing__listing_private_thread']"
    case .starred:
      return "//body[@id='category__topics_listing__listing_topic']"
    case .section:
      return nil
    }
  }

  static func parseThreadNode(_ threadNode: XMLElement, ofType type: ThreadType) -> Thread? {

    if let nameNode = threadNode.at_xpath(".//td[@class='sujetCase3']//a[@class='cCatTopic']"),
       let lastReplyNode = threadNode.at_xpath(".//td[contains(@class, 'sujetCase9')]//a[1]") {

      let lastPageNode = threadNode.at_xpath(".//td[@class='sujetCase4']//a[1]")
      let flagNode = threadNode.at_xpath(".//td[@class='sujetCase5']//a[1]")
      let authorNode = threadNode.at_xpath(".//td[contains(@class, 'sujetCase6')]")
      let answersNode = threadNode.at_xpath(".//td[@class='sujetCase7']")

      //print("Parsing \(nameNode.parent!.content!)")
      
      let thread = Thread(url: nameNode["href"]!,
                          name: nameNode.parent!.content!,
                          lastPageURL: lastPageNode?["href"],
                          lastPostURL: lastReplyNode["href"]!,
                          lastReadURL: flagNode?["href"],
                          lastReadPage: flagNode?["href"]?.firstMatch(forPattern: "page=([0-9]+)"),
                          lastPage: lastPageNode?.text,
                          lastPostDay: lastReplyNode.content!.firstMatch(forPattern: "([0-9]{2}-[0-9]{2}-[0-9]{4})")!.replacingOccurrences(of: "-", with: "/"),
                          lastPostHour: lastReplyNode.content!.firstMatch(forPattern: "([0-9]{2}:[0-9]{2})")!,
                          lastPostAuthor: lastReplyNode.at_xpath(".//b[1]")!.content!,
                          type: type,
                          answers: answersNode?.text,
                          author: authorNode?.text)

      if threadNode.className?.range(of: "ligne_sticky")?.lowerBound != nil {
        thread.sticky = true
      }
      if threadNode.at_xpath(".//img[@alt='closed']") != nil {
        thread.locked = true
      }
      if threadNode.at_xpath(".//td[contains(@class, 'sujetCase1')]//img[@alt='On']") == nil {
        thread.read = true
      }

      let inputNodes = threadNode.xpath(".//td[@class = 'sujetCase10']/input")
      if inputNodes.count == 3 {
        for input in inputNodes {
          if let inputName = input["name"], let inputValue = input["value"] {
            thread.starredInput[inputName] = inputValue
          }
        }
      }

      return thread
    } else {

      return nil
    }
  }

  static func parseStarredThreads(htmlDoc: HTMLDocument) -> Result<ThreadsResults> {
    if let divIdToCheck = MDNParser.authNeeded(for: .starred) {
      if htmlDoc.at_xpath(divIdToCheck) == nil {
        return .error(MDNException.authNeeded)
      }
    }
/*
    if arc4random_uniform(2) == 0 {
      return .error(MDNException.empty)
    }
    if arc4random_uniform(2) == 0 {
      return .error(MDNException.authNeeded)
    }
    if arc4random_uniform(2) == 0 {
      return .error(MDNException.parsing("Doki doki"))
    }
*/
    let form = MDNParser.parseThreadsListForm(htmlDoc)
    MDNParser.parseMessageCount(htmlDoc)

    let favorites = htmlDoc.xpath("//table[@class='main']//tr")
    if favorites.count > 0 {
      var threadSections = [ThreadSection]()
      var threads = [[ThreadID: Thread]]()

      var loopSection: ThreadSection?

      for favorite in favorites {
        if favorite.className?.range(of: "fondForum1fCat")?.lowerBound != nil {

          if let newSection = loopSection, newSection.threadIDs.count > 0 {
            threadSections.append(newSection)
          }

          if let sectionNode = favorite.at_xpath(".//a[@class='cHeader']"), let name = sectionNode.text, let url = sectionNode["href"] {
            loopSection = ThreadSection(section: Section(name: name, url: url), threadIDs: [])
          }

        } else if favorite.className?.range(of: "ligne_booleen")?.lowerBound != nil {

          if let thread = MDNParser.parseThreadNode(favorite, ofType: .starred) {
            threads.append([thread.id: thread])
            loopSection?.threadIDs.append(thread.id)
          }

        }
      }

      if let newSection = loopSection, newSection.threadIDs.count > 0 {
        threadSections.append(newSection)
      }

      if threadSections.count > 0 {
        return .success(ThreadsResults(sections: threadSections, threads: threads, form: form, section: nil))
      } else {
        return .error(MDNException.empty)
      }
    } else {
      return .error(MDNException.empty)
    }
  }

  static func parseThreads(htmlDoc: HTMLDocument, listType: ThreadType, forPage page: String = "1") -> Result<ThreadsResults> {
    if let divIdToCheck = MDNParser.authNeeded(for: listType) {
      if htmlDoc.at_xpath(divIdToCheck) == nil {
        return .error(MDNException.authNeeded)
      }
    }

    let form = MDNParser.parseThreadsListForm(htmlDoc)
    if listType != .message { MDNParser.parseMessageCount(htmlDoc) } // New Message Count is never present on Message's list

    //check current page number

    let threadNodes = htmlDoc.xpath("//tr[contains(@class, 'ligne_booleen')]")
//    var i = 0
    if threadNodes.count > 0 {
      var threads = [[ThreadID: Thread]]()
      var threadSections = [ThreadSection(name: "Page \(page)", threadIDs: [])]

      //current cat ID and subcat ID
      let catInput = htmlDoc.at_xpath("//input[@name='cat']")
      let subCatInput = htmlDoc.at_xpath("//input[@name='subcat']")

      var newSection = Section(name: "<none>", url: "<none>")
      newSection.catID = catInput?["value"]
      newSection.subcatID = subCatInput?["value"]

      let dateStart = Date()
      //let operationQueue = OperationQueue()
      //operationQueue.qualityOfService = .userInitiated
      //operationQueue.maxConcurrentOperationCount = OperationQueue.defaultMaxConcurrentOperationCount
      
      for link in threadNodes {
        //print("=== New Link")
        //let backgroundOperation = BlockOperation {
          if let thread = MDNParser.parseThreadNode(link, ofType: listType) {
            threads.append([thread.id: thread])
            threadSections[0].threadIDs.append(thread.id)
          }
        //}
        //backgroundOperation.queuePriority = .high
        //backgroundOperation.qualityOfService = .userInitiated
        
        //operationQueue.addOperation(backgroundOperation)
        //print("END. Operation Added")
      }
      
      //operationQueue.waitUntilAllOperationsAreFinished()
      
      let dateEnd = Date()
      print("Parse Threads \(dateEnd.timeIntervalSince(dateStart))")
      return .success(ThreadsResults(sections: threadSections, threads: threads, form: form, section: newSection))
    } else {
      return .error(MDNException.empty)
    }
  }

  static func parseThreadsListForm(_ htmlDoc: HTMLDocument) -> [String: String] {
    var inputs = FormInputs()
    let inputNodes = htmlDoc.xpath("//div[@id='mesdiscussions']/input[@type='hidden']")
    for input in inputNodes {

      if let inputName = input["name"], let inputValue = input["value"] {
        inputs[inputName] = inputValue
      }
    }
    return inputs
  }

  static func parseMessageCount(_ htmlDoc: HTMLDocument) {
    var messagesCount:String = "0"
    if let newMessageNode = htmlDoc.at_xpath("//div[@id='mesdiscussions']//a[@class='red']") {
      if let newMessageCount = newMessageNode.text?.firstMatch(forPattern: "([0-9]+)") {
        messagesCount = newMessageCount
      }
    }

    //"MDNUnreadMessagesCount"
    print("new messagesCount \(messagesCount)")
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MDNUnreadMessagesCount"),
                                    object: nil,
                                    userInfo: ["messagesCount": messagesCount])
  }
}
