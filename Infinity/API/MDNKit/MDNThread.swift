//
//  MDNThread.swift
//  Infinity
//
//  Created by FLK on 09/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import PromiseKit
import Kanna
import Foundation

enum ThreadPageType {
  case firstPage
  case lastPage
  case lastRead
  case lastPost
  case current
  case pageNumber(num: String)

  func scrollPosition() -> String? {
    switch self {
    case .firstPage:
      return "top"
    case .lastPage:
      return "top"
    case .lastRead:
      return nil
    case .lastPost:
      return "bottom"
    case .current:
      return nil
    case .pageNumber:
      return "top"
    }
  }
}

typealias PageNumber = String
typealias PageURL = String
typealias ThreadID = String
typealias FormInputs = [String: String]

class Thread {
  lazy var id: ThreadID = {
    return self.parseID(from: url)!
  }()

  var url: PageURL { // this is the Original parsed URL, could be any page
    didSet {
      url = url.replacingOccurrences(of: MDN.client.forumURL!, with: "")
    }
  }
  let type: ThreadType

  // Status
  var locked: Bool = false
  var sticky: Bool = false
  var read: Bool = false

  // Forms URL
  var newAnswerURL: PageURL?          // Parsed on every page
  var starredInput = FormInputs()     // Parsed on Listings, optional
  var fastAnswerInputs = FormInputs() // Parsed on every page, optional

  // Metadata
  var _name: String?
  var _filteredName: String?
  var name: String? {             // Parsed on Listings OR/AND Parsed on every page
    get  {
      return _name
    }
    set {
      _name = newValue
      _filteredName = filteredName()
    }
  }
  var lastPageURL: PageURL?       // Parsed on Listings OR/AND Parsed on every page
  var lastPostURL: PageURL?       // dynamic = lastPageURL + #bas
  var lastReadURL: PageURL?       // Parsed on Listings, optional

  var lastReadPage: PageNumber?      // Parsed on Listings, optional
  var lastPage: PageNumber?          // Parsed on Listings OR/AND Parsed on every page

  var lastPostDay: String?       // Parsed on Listings, optional
  var lastPostHour: String?      // Parsed on Listings, optional
  var lastPostAuthor: String?    // Parsed on Listings, optional

  var author: String?        // Parsed on Listings, optional, on Messages list, author = recipient(s)
  var answersCount: String?  // Parsed on Listings, optional

  var pages = [PageNumber: [PostID]]()
  var posts = [PostID: Post]()

  var pastableURL: String {
    return "\(MDN.client.forumURL!)\(url)"
  }

  init(url: String) {
    self.url = url
    self.type = .section

  }

  init(url: PageURL, name: String, lastPageURL: PageURL?, lastPostURL: PageURL?, lastReadURL: PageURL?, lastReadPage: PageNumber?, lastPage: PageNumber?,
       lastPostDay: String?, lastPostHour: String?, lastPostAuthor: String?, type: ThreadType, answers: String?, author: String?) {

    self.url = url
    self.type = type

    self.name = name

    self.lastPageURL = lastPageURL
    self.lastPostURL = lastPostURL
    self.lastReadURL = lastReadURL
    self.lastReadPage = lastReadPage
    self.lastPage = lastPage
    self.lastPostDay = lastPostDay
    self.lastPostHour = lastPostHour
    self.lastPostAuthor = lastPostAuthor
    self.answersCount = answers
    self.author = author
  }

  func filteredName() -> String? {
    let replacingString = "TU"
    var filteredString = self.name

    let filters = ["topic unique", "T.U.", "topik unique", "toupik ounik", "topique unique",
                   "topic unik", "topik unik", "topic officiel", "TOPIKUNIK"]

    for filter in filters {
      filteredString = filteredString?.replacingOccurrences(of: filter, with: replacingString, options: .caseInsensitive, range: nil)
    }

    return filteredString
  }
  func posts(at page: PageNumber) -> [Post] {
    var posts = [Post]()
    for postID in self.pages[page]!.sorted() {
      if let post = self.posts[postID] {
        posts.append(post)
        self.lastLoadedPost = post.id
      }
    }

    return posts
  }

  func URL(for type: ThreadPageType) -> PageURL? {
    return self.pageURL(for: type)
  }

  func URL(for page: PageNumber) -> PageURL? {

    var pageURL: PageURL?
    let pattern1 = "_[0-9]+_([0-9]+).htm"
    let pattern2 = "forum2\\.php\\?.*page=([0-9]+)"
    //FIXME check wrong format URL (not cat, no post)

    if let matches = url.regexMatch(forPattern: pattern1), matches.count == 1 {
      pageURL = url.replacingOccurrences(of: "_\(matches[0]).htm", with: "_\(page).htm")
    } else if let matches = url.regexMatch(forPattern: pattern2), matches.count == 1 {
      pageURL = url.replacingOccurrences(of: "page=\(matches[0])", with: "page=\(page)")
    }

    return pageURL
  }

  func page(for url: PageURL) -> PageNumber? {

    var pageNumber: PageNumber?
    let pattern1 = "_[0-9]+_([0-9]+).htm"
    let pattern2 = "forum2\\.php\\?.*page=([0-9]+)"
    //FIXME check wrong format URL (not cat, no post)

    if let matches = url.regexMatch(forPattern: pattern1), matches.count == 1 {
      pageNumber = matches[0]
    } else if let matches = url.regexMatch(forPattern: pattern2), matches.count == 1 {
      pageNumber = matches[0]
    }

    return pageNumber
  }

  func updateThread(with thread: Thread) {
    //FIXME check for parsing errors
    self.name = thread.name
    self.lastPage = thread.lastPage
    self.url = thread.url
    self.newAnswerURL = thread.newAnswerURL
    self.fastAnswerInputs = thread.fastAnswerInputs

    //print(self.fastAnswerInputs)
  }

  func parseID(from url: PageURL) -> ThreadID? {

    var threadID: ThreadID?
    let pattern1 = "_([0-9]+)_[0-9]+.htm"
    //let pattern2 = "forum2\\.php\\?.*post=([0-9]+)"
    //FIXME check wrong format URL (not cat, no post)

    if let matches = url.regexMatch(forPattern: pattern1), matches.count == 1 {
      threadID = matches[0]
    } else if let parameters = URLComponents(string: url) {
      threadID = parameters.queryItems?.first(where: { $0.name == "post" })?.value
    }

    return threadID
  }

  var lastLoadedPost: String?

  func loadPage(type: ThreadPageType) -> Promise<String> {
    //print("loadPage = \(number)")
    return Promise { fulfill, reject in

     //FIXME if let pageURL = URL(for: number) {
        print("TH > loadPage, pageURL = \(URL(for: type)!)")

        MDN.client.getPosts(at: URL(for: type)! ).then { result -> Void in
          //FIXME test if page requested == page returned & return page number to use in here instead of pageNumber(for: type)
          print("TH > GetPosts from Thread... at Page \(self.pageNumber(for: type))")
          self.updateThread(with: result.thread)
          self.pages[self.pageNumber(for: type)] = Array(result.posts.keys)
          result.posts.forEach {
            self.posts[$0] = $1
          }

          for k in self.pages.keys {
            print("TH > Pages.key \(k)")
          }

          return fulfill(self.pageNumber(for: type))
        }.catch { error in
          print("TH > Error from Thread...")
          return reject(error)
        }
    //  } else {
    //    reject(MDNException.parsing("Wrong Page URL"))
    //  }

    }

  }

  func removeStarred(withForm baseForm: FormInputs?) -> Promise<String> {
    return Promise { fulfill, reject in
      if let paramForm = baseForm {
        //Merge baseForm with own thread form inputs
        var finalInputs = FormInputs()
        starredInput.forEach { finalInputs[$0] = $1 }

        for (key, value) in paramForm {
          if key == "hash_check" {
            finalInputs[key] = value
          }
        }

        MDN.client.removeStarred(withForm: finalInputs).then { result -> Void in
          return fulfill("OK!")
        }.catch { error in
          return reject(error)
        }
      } else {
        return reject(MDNException.parsing("Err. No form Input"))
      }

    }
  }

  func pageNumber(for pageType: ThreadPageType) -> PageNumber {
    switch pageType {

    case .firstPage:
      return PageNumber("1")
    case .lastPage:
      return self.lastPage ?? self.page(for: self.url)!
    case .lastRead:
      return self.lastReadPage ?? self.page(for: self.url)!
    case .lastPost:
      return self.lastPage ?? self.page(for: self.url)!
    case .current:
      return self.page(for: self.url)!
    case .pageNumber(let num):
      return PageNumber(num)
    }
  }

  func pageURL(for pageType: ThreadPageType) -> PageURL {
    switch pageType {

    case .firstPage:
      return self.URL(for: PageNumber("1"))!
    case .lastPage:
      return self.lastPageURL ?? self.url
    case .lastRead:
      return self.lastReadURL ?? self.url
    case .lastPost:
      return self.lastPostURL ?? self.url
    case .current:
      return self.url
    case .pageNumber(let num):
      return self.URL(for: num)!
    }
  }
}
