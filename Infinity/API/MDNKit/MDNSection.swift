//
//  MDNSection.swift
//  Infinity
//
//  Created by FLK on 09/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

enum SectionFilter: String {
  case all = "0"
  case subscribed = "3"
  case starred = "1"
  case read = "2"

  var description: String {
    switch self {
    case .all: return "Tous"
    case .subscribed: return "Favoris ★"
    case .starred: return "Favoris & Suivis ★+⚑"
    case .read: return "Lus"
    }
  }
  var shortDescription: String {
    switch self {
    case .all: return "Tous"
    case .subscribed: return "Favoris"
    case .starred: return "Favoris & Suivis"
    case .read: return "Lus"
    }
  }
}

struct Section {
  var name: String
  var url: String
  var icon: String?
  var catID: String?
  var subcatID: String?

  init(name: String, url: String, icon: String? = nil) {
    self.name = name
    self.url = url
    self.icon = icon
  }

  func url(for filter: SectionFilter, page: String = "1") -> String? {
    var url: String?
    if self.catID != nil && self.subcatID == nil {
      url = "/forum1.php?config=hfr.inc&cat=\(self.catID!)&page=1&owntopic=\(filter.rawValue)"
    } else if self.catID != nil && self.subcatID != nil {
      url = "/forum1.php?config=hfr.inc&cat=\(self.catID!)&subcat=\(self.subcatID!)&page=1&owntopic=\(filter.rawValue)"
    }

    url = url?.replacingOccurrences(of: "page=1", with: "page=\(page)")
    //print("\(self.catID) || \(self.subcatID) URL for \(filter) = \(url)")
    return url
  }

  mutating func updating(with section: Section) {
    self.catID = section.catID
    self.subcatID = section.subcatID
  }
}
