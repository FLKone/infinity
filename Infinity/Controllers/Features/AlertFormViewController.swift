//
//  AlertFormViewController.swift
//  Infinity
//
//  Created by flk on 20/10/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class AlertFormViewController: UIViewController {

  weak var delegate: ThreadViewControllerDelegate?
  
  let url: String
  var data: AlertFormResults?

  // MARK: - ViewOutlets
  weak var alertFormView: AlertFormView! { return self.view as! AlertFormView }
  
  init(with url: String) {
    self.url = url
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func loadView() {
    view = AlertFormView()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configure()
    
    MDN.client.getAlertFormInputs(with: url).done { results in
      self.data = results
      
      self.alertFormView.bodyTextView.text = self.data?.inputs["raison"]

    }.ensure {
      self.alertFormView.bodyTextView.text = ""
      self.alertFormView.placeholderTextView.isHidden = false
    }.catch { error in
      switch error as! MDNException {
        case .alreadyAlerted:
          let alert = UIAlertController(title: nil, message: "Une alerte a déjà été envoyée aux modérateurs", preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "Genial !", style: .default) { _ in
            self.cancel()
          })
          self.present(alert, animated: true)
        default:
          print(error)
      }
    }
  }
  
  func configure() {
    self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Annuler",
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(AlertFormViewController.cancelConfirmation)
    )
    
    let sendButton = UIBarButtonItem(title: "Alerter",
                                     style: .done,
                                     target: self,
                                     action: #selector(AlertFormViewController.sendConfirmation))
    self.navigationItem.rightBarButtonItem = sendButton

  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  @objc func cancelConfirmation() {

    if self.alertFormView.bodyTextView.text.count > 0 {
      let alert = UIAlertController(title: "Attention !", message: "Le contenu de votre alerte sera perdu", preferredStyle: .alert)
      
      let confirmAction = UIAlertAction(title: "Confirmer", style: .destructive, handler: { _ in
        self.cancel()
      })
      alert.addAction(confirmAction)
      let cancelAction = UIAlertAction(title: "Annuler", style: .cancel, handler: nil)
      alert.addAction(cancelAction)
      
      self.present(alert, animated: true)
      
    } else {
      self.cancel()
    }
  }

  func cancel() {
    UIApplication.shared.sendAction(NSSelectorFromString("resignFirstResponder"), to: nil, from: nil, for: nil)
    self.delegate?.cancel()
    self.dismiss(animated: true)
  }
  
  @objc func sendConfirmation() {
    //if self.alertFormView.bodyTextView.text.count > 0 {
      let alert = UIAlertController(title: "Confirmer l'alerte ?", message: "Cette action est irréversible :o", preferredStyle: .alert)
      
      let confirmAction = UIAlertAction(title: "Oui !", style: .destructive, handler: { _ in
        self.send()
      })
      alert.addAction(confirmAction)
      let cancelAction = UIAlertAction(title: "Non", style: .cancel, handler: nil)
      alert.addAction(cancelAction)
      
      self.present(alert, animated: true)
    //}
  }
  
  func send() {
    self.navigationItem.rightBarButtonItem?.isEnabled = false
    self.navigationItem.title = "Alerte en cours..."
    
    data!.inputs["raison"] = self.alertFormView.bodyTextView.text
    
    UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
    MDN.client.sendAlertForm(with: data!.submitURL, inputs: data!.inputs).done { result in
      usleep(200)
      print("==== PFVC")
      print(result)
      
      let alert = UIAlertController(title: "Hooray !", message: result.message, preferredStyle: UIAlertController.Style.alert)
      self.present(alert, animated: true) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
          self.delegate?.cancel()
          alert.dismiss(animated: true)
          self.dismiss(animated: true)
        }
        
      }
    }.ensure {
      self.navigationItem.rightBarButtonItem?.isEnabled = true
      self.navigationItem.title = ""
    }.catch { error in
      switch error as! MDNException {
      case .native(let msg):
        let alert = UIAlertController(title: "Ooops :(", message: msg, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
          print("Form > OK")
        })
        self.present(alert, animated: true)
      default:
        print("ERROR")
        print(error)
      }
      
    }
  }

  deinit {
    print("AlertFormViewController DEINIT")
  }
  
}
