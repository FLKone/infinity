//
//  FilterListViewController.swift
//  Infinity
//
//  Created by FLK on 30/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class FilterListViewController: UIViewController {
  var tableView = UITableView(frame: .zero, style: .plain)
  weak var delegate: FilterListViewDelegate?

  var listType: ThreadType
  var currentFilter: SectionFilter
  var filterList: [SectionFilter]

  init(listType: ThreadType, currentFilter: SectionFilter) {
    self.filterList = listType.filters()
    self.listType = listType
    self.currentFilter = currentFilter
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    self.tableView.delegate = self
    self.tableView.dataSource = self

    self.title = "Filtres"

    self.tableView.register(BasicTableViewCell.self, forCellReuseIdentifier: "basicCell")

    self.view.addSubview(self.tableView)
    self.tableView.snp.makeConstraints { make in
      make.size.equalTo(self.view)
    }

  }
  override func viewDidAppear(_ animated: Bool) {
    self.preferredContentSize = CGSize(width: 220, height: self.tableView.contentSize.height)
  }
}

// MARK: - UITableViewDataSource
extension FilterListViewController: UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.filterList.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "basicCell", for: indexPath)

    let filter = self.filterList[indexPath.row]

    //cell.selectionStyle = .none
    //cell.isSelected = false
    //cell.accessoryType = .checkmark

   if filter.rawValue == currentFilter.rawValue {
    print("CHECK")
      cell.accessoryType = .checkmark
    } else {
    print("PAS CHECK")
      cell.accessoryType = .none
    }
    //cell.configure(with: section)
    cell.textLabel?.text = filter.description
    return cell
  }

}

// MARK: - UITableViewDelegate
extension FilterListViewController: UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    print("FilterListViewController didSelectRowAt \(indexPath)")
    self.tableView.deselectRow(at: indexPath, animated: true)
    let filter = self.filterList[indexPath.row]
    self.delegate?.selectFilter(filter)
  }

}
