//
//  BBCodeViewController.swift
//  Infinity
//
//  Created by FLK on 13/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

protocol BBCodeViewDelegate: NSObjectProtocol {
  func didSelect(code: BBCode)
}

struct BBCode {
  var name:String
  var code:String
  var openCode:String
  var closeCode:String
}

class BBCodeViewController: UIViewController {

  var collectionView: UICollectionView!
  weak var delegate: BBCodeViewDelegate?

  var bbCodes = [BBCode]()

  override func viewDidLoad() {
    super.viewDidLoad()

    bbCodes.append(BBCode(name: "Bold", code: "BB-Bold", openCode: "[b]", closeCode: "[/b]"))
    bbCodes.append(BBCode(name: "Italic", code: "BB-Italic", openCode: "[i]", closeCode: "[/i]"))
    bbCodes.append(BBCode(name: "Underline", code: "BB-Underline", openCode: "[u]", closeCode: "[/u]"))
    bbCodes.append(BBCode(name: "Strike", code: "BB-Strike", openCode: "[strike]", closeCode: "[/strike]"))
    bbCodes.append(BBCode(name: "Spoiler", code: "BB-Spoiler", openCode: "[spoiler]", closeCode: "[/spoiler]"))
    bbCodes.append(BBCode(name: "Quote", code: "BB-Quote", openCode: "[quote]", closeCode: "[/quote]"))
    bbCodes.append(BBCode(name: "Url", code: "BB-Link", openCode: "[url]", closeCode: "[/url]"))
    bbCodes.append(BBCode(name: "Image", code: "BB-Image", openCode: "[img]", closeCode: "[/img]"))
    bbCodes.append(BBCode(name: "Fixed", code: "BB-Fixed", openCode: "[fixed]", closeCode: "[/fixed]"))

    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

    collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.register(SmileyCollectionViewCell.self,
                            forCellWithReuseIdentifier: "bbCodeCell")

    collectionView.backgroundColor = .white
    collectionView.alwaysBounceVertical = true
    self.view.addSubview(self.collectionView!)
    self.collectionView?.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
    // Do any additional setup after loading the view.

    self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.collectionView.reloadData()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

}

extension BBCodeViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

    return CGSize(width: 30, height: 30)

  }
}

extension BBCodeViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    self.delegate?.didSelect(code: bbCodes[indexPath.row])
  }

  func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath)
    UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
      cell?.backgroundColor = .lightGray
    })
  }

  func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath)
    UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
      cell?.backgroundColor = .white
    })
  }
}

extension BBCodeViewController: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return bbCodes.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = self.collectionView?.dequeueReusableCell(withReuseIdentifier: "bbCodeCell", for: indexPath) as! SmileyCollectionViewCell

    let bbCode = bbCodes[indexPath.row]
    cell.smileyView.image = UIImage(named: bbCode.code)!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    cell.smileyView.tintColor = .black
    return cell
  }
}
