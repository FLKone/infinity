//
//  LoginFormViewController.swift
//  Infinity
//
//  Created by FLK on 10/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
import PromiseKit

class LoginFormViewController: UIViewController {

  weak var delegate: AuthenticationDelegate?

  // MARK: - ViewOutlets
  weak var loginFormView: LoginFormView! { return self.view as! LoginFormView }
  weak var loginTextField: UITextField! { return loginFormView.loginTextField }
  weak var passwordTextField: UITextField! { return loginFormView.passwordTextField }
  weak var loginButton: UIButton! { return loginFormView.loginButton }

  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.

    loginButton.addTarget(self, action: #selector(LoginFormViewController.loginButtonPressed), for: UIControl.Event.touchUpInside)

    self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Annuler",
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(LoginFormViewController.cancel)
    )
  }

  override func loadView() {
    view = LoginFormView()
  }

  override func viewDidAppear(_ animated: Bool) {
    self.loginTextField.becomeFirstResponder()
  }

  @objc func cancel() {
    UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
    self.dismiss(animated: true, completion: nil)
  }

  @objc func loginButtonPressed() {
    print("Login!")

    MDN.client.login(user: loginTextField.text!, password: passwordTextField.text!).done { result in
      print("Result \(result)")
      self.delegate?.logged()
      self.cancel()
    }.catch { error in
      print("Error \(error)")
    }

  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}
