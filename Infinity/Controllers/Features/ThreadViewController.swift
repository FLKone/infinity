//
//  ThreadViewController.swift
//  Infinity
//
//  Created by FLK on 07/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
import WebKit
import FontAwesome_swift
import SafariServices
import SVPullToRefresh

protocol ThreadViewControllerDelegate: NSObjectProtocol {
  func refresh(withNewScrollPosition: String)
  func cancel()
}

class ThreadViewController: UIViewController {
  //TODO preconfigure ThreadView with empty body
  //stackoverflow link:

  weak var delegate: MasterViewControllerDelegate?
  var thread: Thread
  //var posts: [Post]?    //String: PageNumber, [Post]: Posts |[String: [Post]]| // FIXME
  var liveScrollPosition: String? //Tracking live scrollPosition (Post.id), used after a refresh/reload // FIXME

  var initialPageType: ThreadPageType
  var currentPageNumber: PageNumber
    
  var webView: PostWebView?
  var pageLabel = UILabel(frame: .zero)
  var titleLabel = ThreadTitleLabel(frame: .zero)

  init(thread: Thread, initialPageType: ThreadPageType = .current) {
    self.initialPageType = initialPageType
    self.thread = thread
    self.currentPageNumber = thread.pageNumber(for: initialPageType)
    
    self.liveScrollPosition = initialPageType.scrollPosition()
    //self.customInput = customInputController.accessoryView!

    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  var shouldBecomeFirstResponder = false
  override var canBecomeFirstResponder: Bool {
    print("canBecomeFirstResponder")
    return shouldBecomeFirstResponder
  }
  var customInput = ThreadAccessoryView(frame: CGRect(x: 0, y: 0, width: 0, height: 50))

  override var inputAccessoryView: ThreadAccessoryView? {
    //print("inputAccessoryView")
    //print("customInput del \(customInput.postTextField.delegate)")
    //customInput.backgroundColor = .white
    if shouldBecomeFirstResponder {
      return customInput
    } else {
      return nil
    }

  }

  func loadPage(type: ThreadPageType) {
    print("TV > loadCurrentPage.....")

    titleLabel.text = "Chargement de la page nº\(thread.pageNumber(for: type))..."
    titleLabel.sizeToFit()

    thread.loadPage(type: type).done { result in
      print("TV > Get Posts OK.... loading of page=\(result)")

      self.currentPageNumber = String(result)

      //FIXME Scroll position
      //print(self.thread.pages.values)
      //let fileManager = FileManager.default
      //let cacheDirectoryURL = fileManager.urls(for: .itemReplacementDirectory, in: .userDomainMask).first!
      //let avatarDirectoryURL = cacheDirectoryURL.appendingPathComponent("MDN-Avatars", isDirectory: true)
      let bundlePath = Bundle.main.bundlePath
      let bundleUrl = URL(fileURLWithPath: bundlePath, isDirectory: true)

      self.webView?.loadHTMLString(Template.html.thread(self.thread.posts(at: result)), baseURL: bundleUrl)
      self.configureToolBar()

    }.ensure {
      if let refreshView = self.webView?.scrollView.pullToRefreshView {
        refreshView.stopAnimating()
      }
    }.catch { error in
      print(error)
    }
  }

  deinit {
    print("dealloc Thread View")
    self.webView?.stopLoading()
    self.webView?.navigationDelegate = nil
    self.delegate = nil
    self.webView = nil
  }
  override func viewWillDisappear(_ animated: Bool) {
    print("viewWillDisappear")
    super.viewWillDisappear(animated)
    self.disableScrolling()
    //self.tabBarController?.tabBar.isHidden = false
  }
  override func viewWillAppear(_ animated: Bool) {
    print("viewWillAppear")

    super.viewWillAppear(animated)
    self.enableScrolling()
    //self.tabBarController?.tabBar.isHidden = true
  }
  @objc func adjustForKeyboard(notification: Notification) {
    let userInfo = notification.userInfo!

    let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

    if notification.name == UIResponder.keyboardWillHideNotification {
      print("adjustForKeyboard Keyboard will Hide")
      
      //FIXME Swift 4.2 bug https://bugs.swift.org/browse/SR-7879
      webView?.scrollView.contentInset = .init()
      
      let bottomOffset = CGPoint(x: 0,
                                 y: (webView?.scrollView.contentOffset.y)!)
      webView?.scrollView.setContentOffset(bottomOffset, animated: true)

    } else {
      print("adjustForKeyboard Keyboard will NOT Hide \(notification.name)")

      webView?.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height-50, right: 0)

    }

    webView?.scrollView.scrollIndicatorInsets = (webView?.scrollView.contentInset)!

    /*
    let selectedRange = postFormView.bodyTextView.selectedRange
    postFormView.bodyTextView.scrollRangeToVisible(selectedRange)
     */

  }

  @objc func adjustWebViewForKeyboard(notification: Notification) {
    //let userInfo = notification.userInfo!

    //let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    //let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

    if notification.name == UIResponder.keyboardDidHideNotification {

    } else {
      print("adjustWebViewForKeyboard Keyboard will Show \(notification.name)")

      //print("window.scrollBy(0, \(keyboardViewEndFrame.height) custom \(customInput.frame.size.height)")
      //window.scrollBy(0, \(keyboardViewEndFrame.height))
      //if keyboardViewEndFrame.height > customInput.frame.size.height {
        let bottomOffset = CGPoint(x: 0,
                                   y: (webView?.scrollView.contentSize.height)! - (webView?.scrollView.bounds.size.height)! + (webView?.scrollView.contentInset.bottom)!)
        webView?.scrollView.setContentOffset(bottomOffset, animated: true)

        //webView?.evaluateJavaScript("document.getElementById('mdn_bottom').scrollIntoView(true);", completionHandler: nil)
      //}
    }

  }

  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = UIColor(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 1)
    print("main did load")
    //self.tabBarController?.tabBar.isHidden = true
    //notificationCenter.addObserver(self, selector: #selector(adjustWebViewForKeyboard), name: .UIKeyboardDidHide, object: nil)

    /*
     TODO
     let source = "document.body.style.background = \"#777\";"
     let userScript = WKUserScript(source: source, injectionTime: .AtDocumentEnd, forMainFrameOnly: true)

     let userContentController = WKUserContentController()
     userContentController.addUserScript(userScript)
     */

    //self.wv.configuration.userContentController.addScriptMessageHandler(LeakAvoider(delegate:self), name: "dummy")

    //fa-search
    //fa-pie-chart
    let webConfiguration = WKWebViewConfiguration()
    let contentController = WKUserContentController()
    contentController.add(LeakAvoider(delegate:self), name: "notification")
    webConfiguration.userContentController = contentController
    webConfiguration.preferences.setValue(true, forKey: "allowFileAccessFromFileURLs")
    self.webView = PostWebView(frame: .zero, configuration: webConfiguration)
    self.view.addSubview(self.webView!)
    self.webView?.navigationDelegate = self
    self.webView?.snp.makeConstraints { (make) in
      make.edges.equalToSuperview()
    }
    self.webView?.isOpaque = false
    self.webView?.backgroundColor = .clear//UIColor(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 1)
    self.webView?.scrollView.backgroundColor = .clear//UIColor(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 1)
    self.customInput.delegate = self
    self.configureNavigationBarOnce()
    self.loadPage(type: self.initialPageType)
  }

  private func configureNavigationBarOnce() {
    //UIFont.systemFont(ofSize: 17, weight: .heavy)
    titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
    titleLabel.textColor = .black
    titleLabel.numberOfLines = 2
    titleLabel.textAlignment = .center
    //titleLabel.backgroundColor = .red

    navigationItem.titleView = titleLabel
    
    if self.presentingViewController == nil {
      let searchButton = UIBarButtonItem(fontAwesomeName: "fa-search",
                                         target: self,
                                         action: nil,
                                         location: .navigationBar)
      searchButton.isEnabled = false
      self.navigationItem.leftBarButtonItem = searchButton
      
      if thread.type == .message {
        let unreadButton = UIBarButtonItem(fontAwesomeName: "fa-eye-slash",
                                           target: self,
                                           action: nil,
                                           location: .navigationBar)
        unreadButton.isEnabled = false
        self.navigationItem.rightBarButtonItem = unreadButton
        
      } else {
        let pollButton = UIBarButtonItem(fontAwesomeName: "fa-pie-chart",
                                         target: self,
                                         action: nil,
                                         location: .navigationBar)
        pollButton.isEnabled = false
        self.navigationItem.rightBarButtonItem = pollButton
      }
    } else {
      let closeButton = UIBarButtonItem(fontAwesomeName: "fa-window-close",
                                         target: self,
                                         action: #selector(ThreadViewController.close),
                                         location: .navigationBar)
      self.navigationItem.rightBarButtonItem = closeButton
    }
    
  }

  @objc private func close() {
    self.dismiss(animated: true)
  }
  
  private func configureToolBar() {
    let attributes = [NSAttributedString.Key.font: UIFont.fontAwesome(ofSize: 20)] as [NSAttributedString.Key: Any]
    let smallerAttributes = [NSAttributedString.Key.font: UIFont.fontAwesome(ofSize: 13)] as [NSAttributedString.Key: Any]

    let flexibleSpaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let fixedSpaceItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
    fixedSpaceItem.width = 10

    let previousPageItem = UIBarButtonItem(title: String.fontAwesomeIcon(code: "fa-angle-up"),
                                           style: .plain, target: self, action: #selector(loadPreviousPage))

    let nextPageItem = UIBarButtonItem(title: String.fontAwesomeIcon(code: "fa-angle-down"),
                                       style: .done, target: self, action: #selector(loadNextPage))

    if currentPageNumber == "1" {
      previousPageItem.isEnabled = false
    }
    if currentPageNumber == thread.pageNumber(for: .lastPage) {
      nextPageItem.isEnabled = false
    }

    previousPageItem.setTitleTextAttributes(attributes: attributes)
    nextPageItem.setTitleTextAttributes(attributes: attributes)
    
    pageLabel.text = "Page : \(self.currentPageNumber) / \(thread.pageNumber(for: .lastPage))"
    pageLabel.sizeToFit()
    pageLabel.textColor = .black
    pageLabel.font = UIFont.systemFont(ofSize: 11, weight: .light)
    pageLabel.backgroundColor = .clear
    pageLabel.textAlignment = .center
    
    let pageLabelItem = UIBarButtonItem(customView: pageLabel)

    var toolbarItems = [previousPageItem, fixedSpaceItem, nextPageItem,
                        flexibleSpaceItem, flexibleSpaceItem, pageLabelItem, flexibleSpaceItem, flexibleSpaceItem]
    
    if thread.newAnswerURL != nil {
      let answerPageItem = UIBarButtonItem(title: String.fontAwesomeIcon(code: "fa-share"), style: .plain, target: nil, action: nil)
      let fullAnswerPageItem = UIBarButtonItem(title: String.fontAwesomeIcon(code: "fa-window-maximize"),
                                               style: .plain,
                                               target: self, action: #selector(showAnswerForm))
      let fastAnswerPageItem = UIBarButtonItem(title: String.fontAwesomeIcon(code: "fa-window-minimize"),
                                               style: .plain,
                                               target: self, action: #selector(showFastAnswerForm))
      
      answerPageItem.isEnabled = false
      
      answerPageItem.setTitleTextAttributes(attributes: smallerAttributes)
      fullAnswerPageItem.setTitleTextAttributes(attributes: smallerAttributes)
      fastAnswerPageItem.setTitleTextAttributes(attributes: smallerAttributes)
      
      toolbarItems.append(answerPageItem)
      toolbarItems.append(fastAnswerPageItem)
      toolbarItems.append(fullAnswerPageItem)

    } else {
      let closedItem = UIBarButtonItem(title: String.fontAwesomeIcon(code: "fa-lock"),
                                       style: .plain,
                                       target: self, action: #selector(showClosedThreadAlert))

      closedItem.setTitleTextAttributes(attributes: smallerAttributes)
      toolbarItems.append(closedItem)

    }

    self.setToolbarItems(toolbarItems, animated: false)
    /*
    let refreshPageItem = UIBarButtonItem(title: String.fontAwesomeIcon(code: "fa-refresh"), style: .plain, target: self, action: #selector(reloadPage))

    refreshPageItem.setTitleTextAttributes(attributes: attributes)
    self.navigationItem.rightBarButtonItem = refreshPageItem
    */

    self.navigationController?.setToolbarHidden(false, animated: false)
    self.navigationController?.toolbar.isTranslucent = false
  }

  @objc func loadPreviousPage() {
    self.liveScrollPosition = "bottom"
    self.loadPage(type: .pageNumber(num: "\(Int(self.currentPageNumber)!-1)"))
  }
  @objc func loadNextPage() {
    self.liveScrollPosition = "top"
    self.loadPage(type: .pageNumber(num: "\(Int(self.currentPageNumber)!+1)"))
  }

  @objc func showFastAnswerForm() {
    self.shouldBecomeFirstResponder = true
    self.becomeFirstResponder()
    //print(self.inputAccessoryView?.postTextField)
    self.inputAccessoryView?.postTextField.becomeFirstResponder()
    //self.inputAccessoryView?.postTextField.becomeFirstResponder()
    //self.customInputController.becomeFirstResponder()
    //self.customInputController.accessoryView.becomeFirstResponder()
    //self.customInputController.accessoryView.becomeFirstResponder()
    //self.customInput.postTextField.becomeFirstResponder()
    //print("sfa del = \(self.customInput.postTextField.delegate)")

  }

  @objc func showClosedThreadAlert() {
    //FIXME Specific message for deleted private message
    //FIXME Display message for direct answer/multiquote
    let alert = UIAlertController(title: "Ooops :(",
                                  message: "Il n'est pas possible de répondre (Sujet fermé)",
                                  preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "D'accord !", style: .cancel))
    self.present(alert, animated: true)
  }
  
  @objc func showAnswerForm() {

    if let newURL = thread.newAnswerURL {
      //print(nau)
      let vc = PostFormViewController(for: .creation, with: newURL)
      vc.delegate = self
      self.disableScrolling()
      vc.fastAnswer = self.customInput.postTextField.text
      self.customInput.postTextField.text = ""
      self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
  }
  @objc func reloadPage() {

    //FIXME Get current flag to reposition scroll after
    //print(self.thread.lastLoadedPost)
    self.liveScrollPosition = "post\(self.thread.lastLoadedPost ?? "bottom")"
    //print("Reload > \(self.liveScrollPosition)")
    self.loadPage(type: .pageNumber(num: self.currentPageNumber))
  }

  // MARK: - Contextual Menu
  func hideContextualMenu() {
    let menuController = UIMenuController.shared
    
    if menuController.isMenuVisible {
      menuController.setMenuVisible(false, animated: true)
    }
  }

  func showContextualMenu(for id: String, at point: CGPoint) {
    //print("id = \(id) | yPos = \(yPos) \(self.webView!.safeAreaInsets.top)")
    let curPostID = Int(id)!
    let post = self.thread.posts[id]
    let menuController = UIMenuController.shared
    
    // --- START
    menuController.menuItems = []

    if let editURL = post?.editURL {
      // 1- Edit
      // 2- Delete
      let optionEditItem = UIMenuItem(title: "optionEditItem", image: UIImage(named: "Post-Edit-New")) { [unowned self] menuItem in
        self.showForm(for: .edition, with: editURL)
      }
      menuController.menuItems?.append(optionEditItem)

      let optionDeleteItem = UIMenuItem(title: "optionDeleteItem", image: UIImage(named: "Post-Delete-New")) { [unowned self] menuItem in
        self.showForm(for: .deletion, with: editURL)
      }
      menuController.menuItems?.append(optionDeleteItem)

    } else {
      // 1- Profile (shortcut on message author) (blacklist on profile page)
      // 2- Private Message
      if let userProfileURL = post?.userProfileURL {
        let optionProfileItem = UIMenuItem(title: "optionProfileItem", image: UIImage(named: "Post-Profile-New")) { menuItem in
          self.showUserProfile(with: userProfileURL)
        }
        menuController.menuItems?.append(optionProfileItem)
      }
      
      if let privateMessageURL = post?.privateMessageURL {
        let optionMessageItem = UIMenuItem(title: "optionMessageItem", image: UIImage(named: "Post-Message-New")) { menuItem in
          self.showMessageForm(with: privateMessageURL)
        }
        menuController.menuItems?.append(optionMessageItem)
      }
    }

    // 3- Quote
    if let quoteURL = post?.quoteURL {
      let optionQuoteItem = UIMenuItem(title: "optionQuoteItem", image: UIImage(named: "Post-Quote")) { [unowned self] menuItem in
        self.showForm(for: .creation, with: quoteURL)
      }
      menuController.menuItems?.append(optionQuoteItem)
    }
    
    // 4- Multiquote
    if let cookieName = post?.multiQuoteString { //FIXME Hide cookie access in MDNKit
      let cookieValue = MDN.client.readCookie(name: cookieName)
      
      var imageMultiquote = UIImage(named: "Post-Multiquote")
      if cookieValue.range(of: id) != nil {
        imageMultiquote = UIImage(named: "Post-Multiquote_On")
      }
      
      let optionMultiquoteItem = UIMenuItem(title: "optionMultiquoteItem", image: imageMultiquote) { [unowned self] menuItem in
        self.quotePost(curPostID)
      }
      menuController.menuItems?.append(optionMultiquoteItem)
    }

    // 5- Link (shortcut on message humor)
    if let postURL = post?.url {
      let optionLinkItem = UIMenuItem(title: "optionLinkItem", image: UIImage(named: "Post-Link")) { menuItem in
        self.copyLink(with: postURL)
      }
      menuController.menuItems?.append(optionLinkItem)
    }

    // 6- Alert
    if let alertURL = post?.alertURL {
      let optionAlertItem = UIMenuItem(title: "optionAlertItem", image: UIImage(named: "Post-Alert")) { menuItem in
        print("optionAlertItem !")
        self.showAlertForm(with: alertURL)
      }
      menuController.menuItems?.append(optionAlertItem)
    }

    // 7- Star
    if post?.starringPostURL != nil {
      let optionStarItem = UIMenuItem(title: "optionStarItem", image: UIImage(named: "Post-Star")) { menuItem in
        self.starPost(curPostID)
      }
      menuController.menuItems?.append(optionStarItem)
    }
    // --- END
    
    var finalY: CGFloat
    
    finalY = self.webView!.scrollView.contentInset.top + point.y
    
    let menuRect = CGRect(x: point.x,
                          y: finalY,
                          width: 1,
                          height: 55)
    if point.y < 50 {
      menuController.arrowDirection = .up
    } else {
      menuController.arrowDirection = .down
    }
    self.webView?.isCustomMenuVisible = true
    
    menuController.setTargetRect(menuRect, in: self.webView!)
    menuController.setMenuVisible(true, animated:true)

  }
  // MARK: - Contextual Options
  override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
    return false
  }

  func showForm(for mode: FormMode, with url: String) {
    UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
    
    let vc = PostFormViewController(for: mode, with: url)
    vc.delegate = self
    self.disableScrolling()
    self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
  }

  func quotePost(_ postID: Int) {
    let wantedPostID = String(postID)
    
    if let post = self.thread.posts[wantedPostID] {
      if let cookieName = post.multiQuoteString {
        var cookieValue = MDN.client.readCookie(name: cookieName)
        
        if cookieValue.range(of: wantedPostID) == nil {
          cookieValue.append("|\(wantedPostID)")
        } else {
          cookieValue = cookieValue.replacingOccurrences(of: "|\(wantedPostID)", with: "")
        }
        
        MDN.client.writeCookie(name: cookieName, with: cookieValue)
      }
    }
  }
  
  func starPost(_ postID: Int) {

    let wantedPostID = String(postID)
    
    let tempTitle = titleLabel.text
    titleLabel.text = "Ajout aux favoris en cours..."
    titleLabel.sizeToFit()
    
    if let post = self.thread.posts[wantedPostID] {
      post.starring().done { result in
        let alert = UIAlertController(title: "Hooray !", message: "Favori ajouté avec succès", preferredStyle: .alert)
        self.present(alert, animated: true) {
          DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
            alert.dismiss(animated: true)
          }
        }
      }.ensure {
        self.titleLabel.text = tempTitle
        self.titleLabel.sizeToFit()
      }.catch { _ in
        let alert = UIAlertController(title: "Ooops :(",
                                      message: "Problème survenu lors de l'ajout aux favoris.\nActualisez la liste et réessayez.",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "D'accord !", style: .cancel))
        self.present(alert, animated: true)
      }
    }

  }
  
  func showUserProfile(with url: String) {
    print("showUserProfile \(url)")
    
    UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
    
    let vc = UserProfileViewController(with: url)
    //vc.delegate = self
    self.disableScrolling()
    self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
  }
  func showMessageForm(with url: String) {
    print("showMessageForm \(url)")
    
    UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
    
    let vc = PostFormViewController(for: .newMessage, with: url)
    vc.delegate = self
    self.disableScrolling()
    self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
  }
  func showAlertForm(with url: String) {
    print("showAlertForm \(url)")
    
    UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
    
    let vc = AlertFormViewController(with: url)
    vc.delegate = self
    self.disableScrolling()
    self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
  }
  func copyLink(with url: String) {
    print("copyLink \(url)")
  }
  //TODO: - Deal with MEMORY dude!

}

// MARK: - WKWebViewDelegate
extension ThreadViewController: WKNavigationDelegate {

  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    print("webView didFinish \(navigation)")
    let scrollPosition = liveScrollPosition ?? self.thread.url.scrollPosition()
    print("scrollP = \(scrollPosition)")
    self.webView?.evaluateJavaScript("""
        document.getElementById('mdn_bottom').scrollIntoView(true);
        document.getElementById('mdn_\(scrollPosition)').scrollIntoView(true);
        document.getElementById('mdn_\(scrollPosition)').classList.add('last_read');
      """)
    //FIXME pos == top > Hidden by NavigationBar
    titleLabel.text = thread.filteredName()
    titleLabel.sizeToFit()
    self.webView?.scrollView.flashScrollIndicators()



    if self.currentPageNumber == self.thread.pageNumber(for: .lastPage) {
      self.webView?.scrollView.addPull(toRefresh: SVArrowPullToRefreshView.self, withActionHandler: { [unowned self] in
        self.reloadPage()
      }, position: .bottom)
    } else {
      self.webView?.scrollView.addPull(toRefresh: SVArrowPullToRefreshView.self, withActionHandler: { [unowned self] in
        self.loadNextPage()
      }, position: .bottom)

      if let arrowRefreshView = self.webView?.scrollView.pullToRefreshView as? SVArrowPullToRefreshView {
        arrowRefreshView.setTitle("Tirer pour afficher la page suivante...", for: .stopped)
        arrowRefreshView.setTitle("Lâcher pour afficher la page suivante...", for: .triggered)
      }
    }
  }
 
  func webView(_ webView: WKWebView,
               decidePolicyFor navigationAction: WKNavigationAction,
               decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {

    //print("Policy For \(navigationAction.request.url)")
    //print("Type For \(navigationAction.navigationType)")

    if navigationAction.navigationType == .other {
      decisionHandler(.allow)
    } else {
      decisionHandler(.cancel)
      let actionURL = navigationAction.request.url
      let result = Link.helper.type(of: actionURL)
      switch result.type {
      case .error:
        break
      case .thread:
        // Allow user to close presented controller
        let threadVC = ThreadViewController(thread: Thread(url: result.url), initialPageType: .current)
        if self.presentingViewController != nil {
          // self is being presented, push on navigation stack
          self.navigationController?.pushViewController(threadVC, animated: true)
        } else {
          let pNC = PopNavigationController(rootViewController: threadVC)
          self.present(pNC, animated: true, completion: nil)
        }
      case .profile:
        break
      case .section:
        break
      case .unknown:
        break
      case .external:
        if self.presentingViewController != nil {
          UIApplication.shared.open(URL(string: result.url)!)
        } else {
          let safariVC = SFSafariViewController(url: URL(string: result.url)!)
          UIApplication.shared.delegate?.window!?.rootViewController?.present(safariVC, animated: true, completion: nil)
        }
      }
    }
  }

}

// MARK: - WKScriptMessageHandler
extension ThreadViewController: WKScriptMessageHandler {

  func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    print(message.body)
    //FIXME >>> get id&yPos from MDN API
    if let dico = message.body as? [String: Any] {
      print("dico = \(dico)")
      if let type = dico["type"] as? String {
        if type == "touch" {
          self.hideContextualMenu()
        } else if type == "menu" {
          if let id = dico["id"] as? String, let yPos = dico["yPos"] as? CGFloat, let clientX = dico["clientX"] as? CGFloat {
            self.showContextualMenu(for: id, at: CGPoint(x: clientX, y: yPos))
          }
        }
      }
    }
  }
}

// MARK: - SwipablePanel
extension ThreadViewController: SwipablePanel {
  func enableScrolling() {
    DispatchQueue.main.async {
      self.webView?.scrollView.panGestureRecognizer.isEnabled = true
      self.webView?.isUserInteractionEnabled = true
      //TODO: Disable long-press gesture (text-selection / contextual menu)
      self.webView?.addObservers()
      //self.becomeFirstResponder()
      //self.webView?.becomeFirstResponder()
      
      let notificationCenter = NotificationCenter.default
      notificationCenter.addObserver(self, selector: #selector(self.adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
      notificationCenter.addObserver(self, selector: #selector(self.adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
      notificationCenter.addObserver(self, selector: #selector(self.adjustWebViewForKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
  }

  func disableScrolling() {
    DispatchQueue.main.async {
      self.webView?.scrollView.panGestureRecognizer.isEnabled = false
      self.shouldBecomeFirstResponder = false
      self.webView?.isUserInteractionEnabled = false
      self.webView?.removeObservers()
      self.customInput.postTextField.resignFirstResponder()
      self.resignFirstResponder()

      let notificationCenter = NotificationCenter.default
      notificationCenter.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
      notificationCenter.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
      notificationCenter.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
  }

}

// MARK: ThreadViewControllerDelegate
extension ThreadViewController: ThreadViewControllerDelegate {
  func refresh(withNewScrollPosition scrollPosition: String) {
    self.enableScrolling()
    self.liveScrollPosition = "post\(scrollPosition)"
    self.loadPage(type: .pageNumber(num: self.currentPageNumber))

    // remove current text in fastAnswer
    self.customInput.postTextField.text = ""

    // delete cookie
    if let post = self.thread.posts.first?.value {
      if let cookieName = post.multiQuoteString {
        MDN.client.deleteCookie(name: cookieName)
      }
    }

    //FIXME refresh page after

  }

  func cancel() {
    self.enableScrolling()
  }

}

// MARK: ThreadAccessoryDelegate
extension ThreadViewController: ThreadAccessoryDelegate {
  func shouldAcceptTextChanges(text: String) {

    let tempTitle = titleLabel.text
    titleLabel.text = "Envoi en cours..."
    titleLabel.sizeToFit()

    var formInputs = self.thread.fastAnswerInputs
    formInputs["content_form"] = text

    self.shouldBecomeFirstResponder = false
    self.webView?.becomeFirstResponder()

    MDN.client.sendPostForm(for: .creation, with: formInputs).done { result in
      usleep(200)
      print("==== PFVC")
      print(result)

      let alert = UIAlertController(title: "Hooray !", message: result.message, preferredStyle: UIAlertController.Style.alert)

      self.inputAccessoryView?.postTextField.text = ""

      self.present(alert, animated: true) {

        self.refresh(withNewScrollPosition: result.anchor ?? "")

        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(50)) {
          alert.dismiss(animated: true)
        }

      }
    }.ensure {
      self.titleLabel.text = tempTitle
      self.titleLabel.sizeToFit()
    }.catch { error in

      switch error as! MDNException {
      case .parsing(let msg):
        let alert = UIAlertController(title: "Ooops :(", message: msg, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
          print("Form > OK")
          self.showFastAnswerForm()
          //self.inputAccessoryView?.postTextField.becomeFirstResponder()
        })
        alert.addAction(UIAlertAction(title: "Ressayer (PH)", style: .cancel) { _ in
          self.showFastAnswerForm()
          print("Form > Ressayer")
        })
        alert.addAction(UIAlertAction(title: "Annuler", style: .destructive) { _ in
          self.dismiss(animated: true)
        })
        self.present(alert, animated: true)
      default:
        print("ERROR")
        print(error)
      }

    }
  }

  func shouldDismissTextChanges() {

  }
}
