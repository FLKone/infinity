//
//  MasterViewController.swift
//  Infinity
//
//  Created by FLK on 07/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
import SnapKit

enum SlideOutState {
  case bothCollapsed
  case leftPanelExpanded
  case rightPanelExpanded
}

enum Panel {
  case right
  case left
  case none
}

protocol SwipablePanel {
  func enableScrolling()
  func disableScrolling()
}

protocol MasterViewControllerDelegate: NSObjectProtocol {
  func toggleLeftPanel()
  func toggleRightPanel()
}

// MARK: MasterViewController
class MasterViewController: UIViewController {

  let swipeTreshold: CGFloat = 15

  var currentState: SlideOutState = .bothCollapsed
  var swipedPanel: Panel = .none

  //var leftNavigationController: UIViewController?
  var leftViewController: ThreadSelectionTabBarController?
  var rightViewController: TabsViewController?

  var mainNavigationController: UINavigationController?
  var mainViewController: ThreadViewController?
  var overlay: UIView?

  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = .darkGray

    let versionLabel = UILabel()
    var versionString = "HFR+ 2.0"
    if let _ = Bundle.main.infoDictionary?["CFBundleShortVersionString"], let build = Bundle.main.infoDictionary?["CFBundleVersion"] {
      versionString += " #\(build)"
    }

    versionLabel.text = versionString
    versionLabel.textColor = .lightGray
    versionLabel.sizeToFit()

    self.view.addSubview(versionLabel)
    versionLabel.snp.makeConstraints { make in
      make.center.equalTo(self.view)
    }

    self.overlay = UIView(frame: .zero)
    self.overlay?.backgroundColor = UIColor.black.withAlphaComponent(0)
    self.overlay?.isUserInteractionEnabled = false

    view.addSubview(self.overlay!)
    self.overlay?.snp.makeConstraints { make -> Void in
      make.edges.equalToSuperview()
    }

    // Left Side Controller (Thread Selection)
    let tabsC = ThreadSelectionTabBarController()//ThreadsListViewController(forType: .starred)
    tabsC.threadSelectionDelegate = self
    leftViewController = tabsC//UINavigationController(rootViewController: leftViewController!)

    self.addChild(leftViewController!)
    view.addSubview(leftViewController!.view)
    //print("Original left offset \(-self.view.frame.width)")
    leftViewController!.view.snp.makeConstraints { make -> Void in
      make.size.equalToSuperview()
      make.centerX.equalToSuperview().offset(-self.view.frame.width)
      make.centerY.equalToSuperview()
    }
    leftViewController!.didMove(toParent: self)

    // Right Side Controller (Tabs)
    rightViewController = TabsViewController()
    self.addChild(rightViewController!)
    view.addSubview(rightViewController!.view)
    rightViewController!.view.snp.makeConstraints { make -> Void in
      make.size.equalToSuperview()
      make.centerX.equalToSuperview().offset(self.view.frame.width)
      make.centerY.equalToSuperview()
    }
    rightViewController!.didMove(toParent: self)

    // Gesture-driven navigation between panels
    let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(MasterViewController.handlePanGesture(recognizer:)))
    panGestureRecognizer.delegate = self
    self.view.addGestureRecognizer(panGestureRecognizer)

    leftViewController!.addObserver(self, forKeyPath: "view.center", options: .new, context: nil)
    rightViewController!.addObserver(self, forKeyPath: "view.center", options: .new, context: nil)

  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    //print("MVC didReceiveMemoryWarning didReceiveMemoryWarning")

    // Dispose of any resources that can be recreated.
  }

  override func viewDidAppear(_ animated: Bool) {

    if currentState == .bothCollapsed && mainViewController == nil {
      //print("viewdidappear, expand left")
      self.toggleLeftPanel()
    }
  }

  override func updateViewConstraints() {
    super.updateViewConstraints()
    leftViewController!.view.snp.updateConstraints { make in
      if currentState == .leftPanelExpanded {
        make.centerX.equalToSuperview()
      } else {
        make.centerX.equalToSuperview().offset(-self.view.frame.width)
      }
    }

    rightViewController!.view.snp.updateConstraints { make in
      if currentState == .rightPanelExpanded {
        make.centerX.equalToSuperview()
      } else {
        make.centerX.equalToSuperview().offset(self.view.frame.width)
      }
    }
  }

  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    //print("willTransition")
    if currentState == .bothCollapsed {
      leftViewController?.view.isHidden = true
      rightViewController?.view.isHidden = true
    } else if currentState == .leftPanelExpanded {
      leftViewController?.view.isHidden = false
      rightViewController?.view.isHidden = true
    } else if currentState == .rightPanelExpanded {
      leftViewController?.view.isHidden = true
      rightViewController?.view.isHidden = false
    }

    coordinator.animate(alongsideTransition: nil) { _ in
      self.leftViewController?.view.isHidden = false
      self.rightViewController?.view.isHidden = false
    }
  }

  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    view.setNeedsUpdateConstraints()
  }

  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    if keyPath == "view.center" {
      if change?[.newKey] as? CGPoint != nil, let uvc = object as? UIViewController {
        let movingFrame = uvc.view.frame

        var progress: CGFloat = 0

        if object is TabsViewController {
          progress = abs((movingFrame.origin.x - movingFrame.width) / movingFrame.width)
        } else  { //if object is UINavigationController
          progress = (movingFrame.origin.x + movingFrame.width) / movingFrame.width
        }

        //print("progress \(progress)")
        mainNavigationController?.view.transform = CGAffineTransform(scaleX: 1*(1-progress/10), y: 1*(1-progress/10))
        mainNavigationController?.view.layer.cornerRadius = 10.0 * progress

        self.overlay?.backgroundColor = UIColor.black.withAlphaComponent(0.7 * progress)
        if progress == 0 {
          mainNavigationController?.view.layer.borderWidth = 0
        } else if self.isAnimatingTranslation {
          mainNavigationController?.view.layer.borderWidth = 1
          uvc.view.layer.shadowOpacity = 0.6
        }
      }
    }
  }

  var horizontalTranslation: CGFloat = 0
  var verticalTranslation: CGFloat = 0
  var isAnimatingTranslation: Bool = false
}

// MARK: UIGestureRecognizerDelegate
extension MasterViewController: UIGestureRecognizerDelegate {

  func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
    //print("gestureRecognizerShouldBegin")
    if let panGesture = gestureRecognizer as? UIPanGestureRecognizer {
      let translation = panGesture.translation(in: view)

      if fabs(translation.y) == 0 {
        mainViewController?.enableScrolling()
        //print("gestureRecognizerShouldBegin true")
        return true
      } else {
        mainViewController?.enableScrolling()
        //print("gestureRecognizerShouldBegin false")
        return false
      }
    } else {
      //print("gestureRecognizerShouldBegin false")
      return false
    }

  }

  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    //print("shouldRecognizeSimultaneouslyWith \(gestureRecognizer) with \(otherGestureRecognizer)")
    return true
  }

  @objc func handlePanGesture(recognizer: UIPanGestureRecognizer) {

    let translation = recognizer.translation(in: view)
    horizontalTranslation += translation.x
    verticalTranslation += translation.y
    recognizer.setTranslation(.zero, in: view)

    //print("gestureRecognizer translation \(translation) hor:\(horizontalTranslation) vert:\(verticalTranslation)")

    switch recognizer.state {
      case .began:
        print("Began!!!")
      case .changed:
        if fabs(horizontalTranslation) < 50 && isAnimatingTranslation == false {
          // Detect current Swipe Direction before the animation is Triggered
          // If one panel is Expanded, swiped panel is forced
          switch currentState {
            case .bothCollapsed:
              if recognizer.velocity(in: view).x > 0 {
                self.swipedPanel = .left
              } else {
                self.swipedPanel = .right
              }
            case .leftPanelExpanded:
              self.swipedPanel = .left
            case .rightPanelExpanded:
              self.swipedPanel = .right
          }

          //print("horizontalTranslation too small >> Break")

          if fabs(verticalTranslation) > 20 {
            //print("Vertical Swipe, canceling Swipe Gesture")
            recognizer.isEnabled = false
          }

          break
        } else if isAnimatingTranslation == false {
          // Animation is Triggered with Haptic feedback, only once.
          // Current gesture needs to be ended before it can be triggered again.
          isAnimatingTranslation = true
          let impact = UIImpactFeedbackGenerator(style: .light)
          impact.impactOccurred()

          leftViewController?.disableScrolling()
          rightViewController?.disableScrolling()
          mainViewController?.disableScrolling()
        }

        // Change Panel's Constraints to reflect current gesture in real time
        animateLivePanel(self.swipedPanel, offset: translation.x)

      case .ended:
        //print("Ended")
        animateForEndedGesture(self.swipedPanel)
        gestureEnded()
      default:
        //print("default :o")
        gestureEnded()
        recognizer.isEnabled = true
    }
  }

  func animateLivePanel(_ panel: Panel, offset: CGFloat) {
    if panel == .left {

      leftViewController!.view.snp.updateConstraints { make in
        //print("New offset \(leftNavigationController!.view.center.x - self.view.center.x + translation.x)")
        make.centerX.equalToSuperview().offset( leftViewController!.view.center.x - self.view.center.x + offset )
      }
      leftViewController!.view.superview?.layoutIfNeeded()
    } else if panel == .right {
      rightViewController!.view.snp.updateConstraints { make in
        make.centerX.equalToSuperview().offset( rightViewController!.view.center.x - self.view.center.x + offset )
      }
      rightViewController!.view.superview?.layoutIfNeeded()
    }
  }

  func animateForEndedGesture(_ panel: Panel) {
    if panel == .left {

      var hasMovedGreaterThanHalfway: Bool
      let leftFrame = leftViewController!.view.frame
      let progress = (leftFrame.origin.x + leftFrame.width) * 100 / leftFrame.width

      if currentState == .leftPanelExpanded {
        mainNavigationController?.view.layer.borderWidth = 0

        hasMovedGreaterThanHalfway = progress > 100 - self.swipeTreshold
      } else {
        mainNavigationController?.view.layer.borderWidth = 1

        hasMovedGreaterThanHalfway = progress > self.swipeTreshold
      }

      animateLeftPanel(shouldExpand: hasMovedGreaterThanHalfway)
    } else if panel == .right {

      var hasMovedGreaterThanHalfway: Bool
      let rightFrame = rightViewController!.view.frame
      let mainFrame = self.view.frame
      let progress = fabs(mainFrame.width - rightFrame.origin.x) * 100 / rightFrame.width

      if currentState == .rightPanelExpanded {
        hasMovedGreaterThanHalfway = progress > 100 - self.swipeTreshold
      } else {
        hasMovedGreaterThanHalfway = progress > self.swipeTreshold
      }

      animateRightPanel(shouldExpand: hasMovedGreaterThanHalfway)
    }
  }

  func gestureEnded() {
    leftViewController?.enableScrolling()
    rightViewController?.enableScrolling()
    mainViewController?.enableScrolling()

    horizontalTranslation = 0
    verticalTranslation = 0
    isAnimatingTranslation = false
    self.swipedPanel = .none
  }
}

// MARK: MasterViewControllerDelegate
extension MasterViewController: MasterViewControllerDelegate {

  func toggleLeftPanel() {
    //print("toggleLeft")
    let notAlreadyExpanded = (currentState != .leftPanelExpanded)

    animateLeftPanel(shouldExpand: notAlreadyExpanded)
  }

  func toggleRightPanel() {
    //print("toggleRight")

    let notAlreadyExpanded = (currentState != .rightPanelExpanded)

    animateRightPanel(shouldExpand: notAlreadyExpanded)
  }

  func animateLeftPanel(shouldExpand: Bool) {
    if shouldExpand {
      //print("Should Expand Left")
      animatePanel(leftViewController!, toXPosition: 0) { _ in
        //print("animateLeftPanel Finished")
        self.currentState = .leftPanelExpanded
        self.leftViewController!.view.layer.shadowOpacity = 0

      }
    } else {
      //print("Should collapse Left")
      animatePanel(leftViewController!, toXPosition: -leftViewController!.view.frame.width) { _ in
        //print("animateLeftPanel ELSE Finished")
        self.currentState = .bothCollapsed
        self.leftViewController!.view.layer.shadowOpacity = 0
      }
    }
  }

  func animateRightPanel(shouldExpand: Bool) {
    if shouldExpand {
      animatePanel(rightViewController!, toXPosition: 0) { _ in
        //print("animateRightPanel Finished")
        self.currentState = .rightPanelExpanded
        self.rightViewController!.view.layer.shadowOpacity = 0
      }
    } else {
      animatePanel(rightViewController!, toXPosition: rightViewController!.view.frame.width) { _ in
        //print("animateRightPanel ELSEFinished")
        self.currentState = .bothCollapsed
        self.rightViewController!.view.layer.shadowOpacity = 0
      }
    }
  }

  func animatePanel(_ panel: UIViewController, toXPosition: CGFloat, completion: ((Bool) -> Void)! = nil) {
    //print("animatePanel to X= \(toXPosition)")

    UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {

      panel.view.snp.updateConstraints { make in
        make.centerX.equalToSuperview().offset(toXPosition)
      }
      panel.view.superview?.layoutIfNeeded()

      if toXPosition == 0 {
        self.overlay?.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.mainNavigationController?.view.transform = CGAffineTransform(scaleX: 1*(1-1/10), y: 1*(1-1/10))
        self.mainNavigationController?.view.layer.cornerRadius = 10.0 * 1

      } else {

        self.overlay?.backgroundColor = UIColor.black.withAlphaComponent(0)
        self.mainNavigationController?.view.transform = CGAffineTransform(scaleX: 1*(1-0/10), y: 1*(1-0/10))
        self.mainNavigationController?.view.layer.cornerRadius = 10.0 * 0

      }

    }, completion: completion)

  }
}

extension MasterViewController: ThreadsListDelegate {

  func selectThread(_ thread: Thread, initialPageType: ThreadPageType = .current) {
    // Main/Center Controller (Thread Viewer)
    //print("MVC > pushThread \(thread) \(initialPageType)")

    if mainViewController != nil {
      //print("MVC > already there, removing")

      //mainViewController?.webView?.configuration.userContentController.removeScriptMessageHandler(forName: "notification")
      mainViewController = nil

      mainNavigationController!.view.removeFromSuperview()
      mainNavigationController?.removeFromParent()
      mainNavigationController = nil

    }

    mainViewController = ThreadViewController(thread: thread, initialPageType: initialPageType)
    mainViewController?.delegate = self

    mainNavigationController = UINavigationController(rootViewController: mainViewController!)

    self.addChild(mainNavigationController!)
    view.insertSubview(mainNavigationController!.view, belowSubview: self.overlay!)

    mainNavigationController?.view.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }

    mainNavigationController?.didMove(toParent: self)

    self.overlay?.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    self.mainNavigationController?.view.transform = CGAffineTransform(scaleX: 1*(1-1/10), y: 1*(1-1/10))
    self.mainNavigationController?.view.layer.cornerRadius = 10.0 * 1
    self.mainNavigationController?.view.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
    self.mainNavigationController?.view.layer.borderWidth = 1 * 1
    self.mainNavigationController?.view.clipsToBounds = true
    //print("MVC > Toggling Left Panel")
    self.toggleLeftPanel()
  }
}
