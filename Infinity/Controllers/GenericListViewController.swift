//
//  GenericListViewController.swift
//  Infinity
//
//  Created by FLK on 08/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
import SVPullToRefresh

extension GenericListViewController: FilterListViewDelegate {
  func selectFilter(_ filter: SectionFilter) {
    print("selectFilter \(filter)")
  }
}

extension GenericListViewController: UIPopoverPresentationControllerDelegate {
  func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
    print("adaptivePresentationStyle \(controller)")
    return UIModalPresentationStyle.none
  }

}
extension GenericListViewController: DropDownTitleDelegate {
  func shouldShowFilterPicker(from sourceView: UIView) {
    print("shouldShowFilterPicker")
    let sc = FilterListViewController(listType: .starred, currentFilter: .starred)
    sc.delegate = self
    let nc = UINavigationController(rootViewController: sc)
    nc.modalPresentationStyle = .popover
    sc.preferredContentSize = CGSize(width: 220, height: 100)

    if let presentation = nc.popoverPresentationController {
      var rect = sourceView.frame
      rect.size.height = 0
      //print("old frame = \(sourceView.frame)")
      //print("New frame = \(self.view.convert(sourceView.superview!.frame, to: self.view))")
      presentation.sourceRect = rect//self.view.convert(sourceView.superview!.frame, to: self.view)
      presentation.sourceView = self.view
      presentation.canOverlapSourceViewRect = false
      presentation.backgroundColor = .white
      presentation.delegate = self
    }

    self.present(nc, animated: true, completion: nil)
  }
}

class GenericListViewController: UIViewController {

  var tableView = UITableView(frame: .zero, style: .plain)

  init() {
    super.init(nibName: nil, bundle: nil)

    let dropDownTitle = DropDownTitleView(title: "Favoris", subtitle: "Tous")
    dropDownTitle.delegate = self
    self.navigationItem.titleView = dropDownTitle

  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }



  override func viewDidLoad() {
    super.viewDidLoad()
    //self.navigationItem.title = "Test"

    self.navigationItem.rightBarButtonItem = UIBarButtonItem(fontAwesomeName: "fa-user-circle",
                                                             target: self,
                                                             action: #selector(showPopup),
                                                             location: .navigationBar)

    self.tableView.delegate = self
    self.tableView.dataSource = self
    self.tableView.register(BasicTableViewCell.self, forCellReuseIdentifier: "basicCell")
    self.tableView.addPull(toRefresh: SVArrowPullToRefreshView.self) {
      DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
        print("done")
        self.tableView.pullToRefreshView.stopAnimating()
      }

    }
    self.view.addSubview(self.tableView)



    self.tableView.snp.makeConstraints { make in
      make.size.equalTo(self.view)
      make.center.equalTo(self.view)
    }

  }
  @objc func showPopup() {
    let lvc = LoginFormViewController()
    let nvc = UINavigationController(rootViewController: lvc)
    //lvc.delegate = self
    nvc.transitioningDelegate = self
    nvc.modalPresentationStyle = .custom
    self.present(nvc, animated: true, completion: nil)
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  let ctt = CustomTopTransition()

}

// MARK: - UIViewControllerTransitioningDelegate
extension GenericListViewController: UIViewControllerTransitioningDelegate {

  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    print("animationController forPresented \(presented) presenting \(presenting)")

    return ctt
  }

  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    print("animationController forDismissed \(dismissed)")

    return CustomTopTransitionReverse()
  }
}

// MARK: - UITableViewDelegate
extension GenericListViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
  }
/*
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    print("scrollViewDidScroll")
  }
 */
}

// MARK: - UITableViewDataSource

extension GenericListViewController: UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return 5
  }

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    print("viewForHeaderInSection \(section)")

    let headerView = UIView()
    headerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
    let newlabel = UILabel()

    newlabel.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    newlabel.textColor = .black
    newlabel.textAlignment = .right
    newlabel.font = newlabel.font.withSize(14)
    newlabel.text = "TEST"

    //headerView.addSubview(newlabel)
    newlabel.sizeToFit()

    let subView = UIView()
    subView.backgroundColor = .red
    headerView.addSubview(subView)

    subView.snp.makeConstraints { make in
      make.size.equalToSuperview()
    }
    return headerView
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 3
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "basicCell", for: indexPath) as! BasicTableViewCell
    //let thread = self.threads[sections[indexPath.section].threadIDs[indexPath.row]]!
    cell.textLabel?.text = "CELL \(indexPath)"
    //cell.configure(with: thread)

    return cell
  }

}
