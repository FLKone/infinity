//
//  ThreadSelectionTabBarController.swift
//  Infinity
//
//  Created by FLK on 29/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
import SnapKit

class ThreadSelectionTabBarController: UITabBarController {
  lazy var sectionsListNavigationController: UINavigationController = {
    let vc = SectionListViewController()
    vc.delegate = self
    let nvc = UINavigationController(navigationBarClass: nil, toolbarClass: NavigationToolBar.self)
    nvc.interactivePopGestureRecognizer?.isEnabled = false
    nvc.viewControllers = [vc]
    nvc.extendedLayoutIncludesOpaqueBars = false
    nvc.tabBarItem = UITabBarItem(title: "Catégories", image: #imageLiteral(resourceName: "Categories"), selectedImage: #imageLiteral(resourceName: "Categories_On")).showingOnlyImage()
    return nvc
  }()

  lazy var starredListNavigationController: UINavigationController = {
    let vc = ThreadsListViewController(forType: .starred)
    vc.delegate = self
    let nvc = UINavigationController(navigationBarClass: nil, toolbarClass: NavigationToolBar.self)
    nvc.interactivePopGestureRecognizer?.isEnabled = false
    nvc.viewControllers = [vc]
    nvc.extendedLayoutIncludesOpaqueBars = false
    nvc.tabBarItem = UITabBarItem(title: "Favoris", image: #imageLiteral(resourceName: "Favoris"), selectedImage: #imageLiteral(resourceName: "Favoris_On")).showingOnlyImage()
    return nvc
  }()

  lazy var messagesListNavigationController: UINavigationController = {
    let vc = ThreadsListViewController(forType: .message)
    vc.delegate = self
    let nvc = UINavigationController(navigationBarClass: nil, toolbarClass: NavigationToolBar.self)
    nvc.interactivePopGestureRecognizer?.isEnabled = false
    nvc.viewControllers = [vc]
    nvc.extendedLayoutIncludesOpaqueBars = false
    nvc.tabBarItem = UITabBarItem(title: "Messages", image: #imageLiteral(resourceName: "MP"), selectedImage: #imageLiteral(resourceName: "MP_On")).showingOnlyImage()
    return nvc
  }()

  weak var threadSelectionDelegate: ThreadsListDelegate?

  init() {
    super.init(nibName: nil, bundle: nil)
    viewControllers = [sectionsListNavigationController, starredListNavigationController, messagesListNavigationController]
    selectedIndex = 1
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    tabBar.isTranslucent = false
    self.view.backgroundColor = .white
    self.delegate = self
    let notificationCenter = NotificationCenter.default
    //notificationCenter.addObserver(self, selector: #selector(self.adjustForKeyboard), name: .UIKeyboardWillHide, object: nil)
    //notificationCenter.addObserver(self, selector: #selector(self.adjustForKeyboard), name: .UIKeyboardWillShow, object: nil)
    notificationCenter.addObserver(self, selector: #selector(self.updateMessageBadge(_:)), name: NSNotification.Name(rawValue: "MDNUnreadMessagesCount"), object: nil)


    //self.view.snp.makeConstraints { make in
    //  make.edges.equalToSuperview()
   // }
  }
/*
  @objc func adjustForKeyboard(notification: Notification) {
    //let userInfo = notification.userInfo!

    //let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    //let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

    if notification.name == Notification.Name.UIKeyboardWillHide {
      self.tabBar.isHidden = false
      if let selectedVC = selectedViewController as? UINavigationController {
        selectedVC.setToolbarHidden(false, animated: true)
      }
    } else {
      self.tabBar.isHidden = true
      if let selectedVC = selectedViewController as? UINavigationController {
        selectedVC.setToolbarHidden(true, animated: true)
      }
    }
    
  }
*/
  @objc func updateMessageBadge(_ notification: NSNotification) {
    //print(notification)
    if let messagesCount = notification.userInfo?["messagesCount"] as? String, messagesCount != "-1" {
      self.messagesListNavigationController.tabBarItem.badgeValue = messagesCount
    }
  }
  /*
  fileprivate lazy var defaultTabBarHeight = { tabBar.frame.size.height }()

  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()

    let newTabBarHeight = defaultTabBarHeight - 10.0

    var newFrame = tabBar.frame
    newFrame.size.height = newTabBarHeight
    newFrame.origin.y = view.frame.size.height - newTabBarHeight

    tabBar.frame = newFrame
  }
 */
  /*
  override func viewDidLayoutSubviews() {
    tabBar.invalidateIntrinsicContentSize()
  }

  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    tabBar.setNeedsLayout()
    tabBar.setNeedsDisplay()
  }
*/
}

extension ThreadSelectionTabBarController: UITabBarControllerDelegate {
  func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    //print("tabBarController should select \(tabBarController.selectedIndex) \(self.selectedIndex)")
    if let vc = viewController as? UINavigationController {
      switch tabBarController.selectedIndex {
      case 1:
        if let threads = vc.topViewController as? ThreadsListViewController, vc == starredListNavigationController {
          threads.fakePull()
        }
      case 2:
        if let threads = vc.topViewController as? ThreadsListViewController, vc == messagesListNavigationController {
          threads.fakePull()
        }
      case 0:
        return true
      default:
        print("Oops")
      }
    }

    return true
  }

}

// MARK: - SwipablePanel
extension ThreadSelectionTabBarController: SwipablePanel {
  func enableScrolling() {
    print("ENABLE Tab Bar Scrolling")
    if let selectedVC = selectedViewController as? UINavigationController {
      if let topVC = selectedVC.topViewController as? ThreadsListViewController {
        topVC.enableScrolling()
      }
    }
  }
  func disableScrolling() {
    print("DISABLE Tab Bar Scrolling")
    if let selectedVC = selectedViewController as? UINavigationController {
      if let threadsVC = selectedVC.topViewController as? ThreadsListViewController {
        threadsVC.disableScrolling()
      } else  if let sectionsVC = selectedVC.topViewController as? SectionListViewController {
        sectionsVC.disableScrolling()
      }
    }
  }

}
extension ThreadSelectionTabBarController: ThreadsListDelegate  {
  func selectThread(_ thread: Thread, initialPageType: ThreadPageType) {
    //if let selectedVC = selectedViewController as? UINavigationController {
      //let threadVC = ThreadViewController(thread: thread, initialPageType: initialPageType)
      //threadVC.hidesBottomBarWhenPushed = true
      self.threadSelectionDelegate?.selectThread(thread, initialPageType: initialPageType)
      //selectedVC.pushViewController(threadVC, animated: true)
    //}
  }

  func selectSection(_ section: Section, filter: SectionFilter) {
    if let selectedVC = selectedViewController as? UINavigationController {
      let threadsList = ThreadsListViewController(with: section, filter: filter)
      threadsList.delegate = self

      selectedVC.pushViewController(threadsList, animated: true)
    }
  }


}
