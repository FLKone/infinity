//
//  TabsViewController.swift
//  Infinity
//
//  Created by FLK on 07/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

//FIXME Should be TabPickerViewController
class TabsViewController: UIViewController {

  weak var delegate: MasterViewControllerDelegate?
  var tableView = UITableView()

  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = .gray

    let versionLabel = UILabel()
    versionLabel.text = "(PH)"
    versionLabel.textColor = .lightGray
    versionLabel.sizeToFit()

    self.view.addSubview(versionLabel)
    versionLabel.snp.makeConstraints { make in
      make.center.equalTo(self.view)
    }
  }

}

// MARK: - UITableViewDataSource
extension TabsViewController: UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 20
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "mainCell", for: indexPath)

    cell.textLabel?.text = "The is right row \(indexPath.row)"
    cell.detailTextLabel?.text = ":o"

    return cell
  }

}

// MARK: - UITableViewDelegate
extension TabsViewController: UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    print("right pushIN")
    self.tableView.deselectRow(at: indexPath, animated: true)

  }

}

// MARK: - SwipablePanel
extension TabsViewController: SwipablePanel {
  func enableScrolling() {
    self.tableView.panGestureRecognizer.isEnabled = true
  }

  func disableScrolling() {
    self.tableView.panGestureRecognizer.isEnabled = false
  }

}
