//
//  BBCodeTextView.swift
//  Infinity
//
//  Created by FLK on 11/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
import MenuItemKit

class BBCodeTextView: UITextView {

  override init(frame: CGRect, textContainer: NSTextContainer?) {
    super.init(frame: frame, textContainer: textContainer)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // Return true if only the BBCode open tag was inserted, false is both open and close tags were inserted (or none)
  func insert(bbcode: BBCode, codeCompleteOnly: Bool = false) -> Bool {
    //FIXME Insert UIPasteboard content

    if let selectedRange = self.selectedTextRange {
      if selectedRange.isEmpty && !codeCompleteOnly {
        self.insertText(bbcode.openCode)

        if let endPosition = self.position(from: selectedRange.end, offset: bbcode.openCode.count) {
          self.selectedTextRange = self.textRange(from: endPosition, to: endPosition)
        }

        return true
      } else {
        if let selectedText = self.text(in: selectedRange) {
          if !codeCompleteOnly {
            self.replace(selectedRange, withText: "\(bbcode.openCode)\(selectedText)\(bbcode.closeCode)")
          } else {
            self.replace(selectedRange, withText: "\(selectedText)\(bbcode.closeCode)")
          }
        }

        return false
      }
    }

    return false
  }
}
