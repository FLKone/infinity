//
//  DropDownTitleView.swift
//  Infinity
//
//  Created by FLK on 03/10/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

protocol DropDownTitleDelegate: NSObjectProtocol {
  func shouldShowFilterPicker(from sourceView: UIView)
}

class DropDownTitleView: UIView {
  var title: String
  var subtitle: String
  var titleButton = UIButton(type: .system)
  //var titleLabel = UILabel(frame: .zero)
  weak var delegate: DropDownTitleDelegate?

  override func tintColorDidChange() {
    switch tintAdjustmentMode {
    case .dimmed:
      print("dimmed")
    default:
      break
    }
  }

  init(title : String, subtitle: String) {
    self.title = title
    self.subtitle = subtitle


    super.init(frame: .zero)

    let titleAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15, weight: .bold),
                           NSAttributedString.Key.foregroundColor: UIColor.black] as [NSAttributedString.Key: Any]

    let selectedFilterAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 11, weight: .medium),
                                    NSAttributedString.Key.foregroundColor: UIColor(red: 36/255, green: 112/255, blue: 216/255, alpha: 1)] as [NSAttributedString.Key: Any]

    let attributedTitle: NSMutableAttributedString = NSMutableAttributedString(string: title, attributes: titleAttributes)
    let attributedFilter: NSAttributedString = NSAttributedString(string: "\n▼ " + subtitle, attributes: selectedFilterAttributes)

    attributedTitle.append(attributedFilter)

    titleButton.setAttributedTitle(attributedTitle, for: UIControl.State.normal)
    titleButton.titleLabel?.numberOfLines = 2
    titleButton.titleLabel?.textAlignment = .center

    titleButton.addTarget(self, action: #selector(showDropDownMenu), for: UIControl.Event.touchUpInside)

    self.addSubview(titleButton)
    titleButton.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }

  }

  func updateSubtitle(newSubtitle: String) {
    if self.subtitle == newSubtitle {
      return
    }
    self.subtitle = newSubtitle
    print("UPDATE SUBS")
    let titleAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15, weight: .heavy),
                           NSAttributedString.Key.foregroundColor: UIColor.black] as [NSAttributedString.Key: Any]

    let selectedFilterAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 11, weight: .medium),
                                    NSAttributedString.Key.foregroundColor: UIColor(red: 36/255, green: 112/255, blue: 216/255, alpha: 1)] as [NSAttributedString.Key: Any]

    let attributedTitle: NSMutableAttributedString = NSMutableAttributedString(string: title, attributes: titleAttributes)
    let attributedFilter: NSAttributedString = NSAttributedString(string: "\n▼ " + subtitle, attributes: selectedFilterAttributes)

    attributedTitle.append(attributedFilter)

    titleButton.setAttributedTitle(attributedTitle, for: UIControl.State.normal)
  }


  @objc func showDropDownMenu() {
    self.delegate?.shouldShowFilterPicker(from: self)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func layoutSubviews() {
  }

}
