//
//  ErrorView.swift
//  Infinity
//
//  Created by FLK on 30/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

typealias ActionHandler = () -> Void

class ErrorView: UIView {
  //weak var delegate: ErrorViewActionDelegate?
  var actionHandler: ActionHandler?
  
  private let imageView: UIImageView = {
    let imageView = UIImageView(frame: .zero)
    imageView.contentMode = .scaleAspectFill
    return imageView
  }()
  
  private let actionButton: UIButton
  
  private let topLabel: UILabel = {
    let label = UILabel()
    label.textColor = .gray
    label.textAlignment = .center
    label.numberOfLines = 1
    label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold)
    label.clipsToBounds = false
    return label
  }()
  
  private let bottomLabel: UILabel = {
    let label = UILabel()
    label.textColor = .gray
    label.textAlignment = .center
    label.numberOfLines = 2
    label.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
    label.clipsToBounds = false
    return label
  }()
  
  fileprivate lazy var stackView: UIStackView = { [unowned self] in
    
    
    let verticalStackView = UIStackView(arrangedSubviews: [self.imageView, self.topLabel, self.bottomLabel, self.actionButton])
    verticalStackView.axis = NSLayoutConstraint.Axis.vertical
    verticalStackView.distribution = UIStackView.Distribution.fill
    verticalStackView.alignment = UIStackView.Alignment.center
    verticalStackView.spacing = 5
    if #available(iOS 11.0, *) {
      verticalStackView.setCustomSpacing(-3, after: self.imageView)
      verticalStackView.setCustomSpacing(20, after: self.bottomLabel)
    }
    return verticalStackView
    }()
  
  init(image: UIImage, title: String, description: String, action: UIButton) {
    self.imageView.image = image
    
    let textAttributes: [NSAttributedString.Key : Any] = [NSAttributedString.Key.baselineOffset: 0]
    self.topLabel.attributedText = NSAttributedString(string: title, attributes: textAttributes)
    self.bottomLabel.attributedText = NSAttributedString(string: description, attributes: textAttributes)
    
    self.actionButton = action
    
    self.actionButton.layer.cornerRadius = 5.0
    
    super.init(frame: .zero)
    
    self.actionButton.sizeToFit()
    self.bottomLabel.sizeToFit()
    
    self.addSubview(self.stackView)
    
    bottomLabel.setContentCompressionResistancePriority(.init(10000), for: .vertical)
    imageView.setContentCompressionResistancePriority(.init(10), for: .vertical)
    
    //self.imageView.backgroundColor = .green
    //self.topLabel.backgroundColor = .green
    //self.bottomLabel.backgroundColor = .green
    //self.actionButton.backgroundColor = .green
    //self.backgroundColor = .red
    
    imageView.snp.makeConstraints { make in
      make.height.equalTo(100)
      make.width.equalTo(100)
    }
    topLabel.snp.makeConstraints { make in
      make.height.equalTo(24)
    }
    //bottomLabel.snp.makeConstraints { make in
    //  make.height.equalTo(60)
    //}
    
    
    actionButton.snp.makeConstraints { make in
      make.height.equalTo(40)
      make.width.equalTo(240)
    }
    
    stackView.snp.makeConstraints { make in
      make.center.equalTo(self)
      make.height.equalTo(230)
      make.width.equalTo(240)
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  @objc func actionForButton() {
    print("actionForButton")
    actionHandler?()
  }
}
