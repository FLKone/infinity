//
//  LoginFormView.swift
//  Infinity
//
//  Created by FLK on 10/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class LoginFormView: UIView {

  // MARK: Variables
  let loginTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Pseudo"
    textField.textColor = .black
    textField.keyboardType = .default
    textField.borderStyle = .roundedRect
    textField.autocapitalizationType = .none
    textField.autocorrectionType = .no
    textField.spellCheckingType = .no
    textField.keyboardAppearance = .default
    textField.returnKeyType = .next
    textField.clearButtonMode = .always
    if #available(iOS 11.0, *) {
      textField.textContentType = .username
    } else {
      // Fallback on earlier versions
    }
    return textField
  }()

  let passwordTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Mot de passe"
    textField.textColor = .black
    textField.keyboardType = .default
    textField.borderStyle = .roundedRect
    textField.autocapitalizationType = .none
    textField.autocorrectionType = .no
    textField.spellCheckingType = .no
    textField.keyboardAppearance = .default
    textField.returnKeyType = .next
    textField.clearButtonMode = .always
    textField.isSecureTextEntry = true
    if #available(iOS 11.0, *) {
      textField.textContentType = .password
    } else {
      // Fallback on earlier versions
    }

    return textField
  }()

  let loginButton: UIButton = {
    let button = UIButton(type: UIButton.ButtonType.system)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 17)
    button.setTitle("Identification", for: UIControl.State.normal)
    //button.setTitleColor(UIColor.white, for: UIControlState.normal)
    //button.backgroundColor = UIColor.clear
    button.layer.borderColor = button.tintColor.cgColor
    button.layer.borderWidth = 1.0

    return button
  }()

  fileprivate lazy var stackView: UIStackView = { [unowned self] in
    let stackView = UIStackView(arrangedSubviews: [self.loginTextField, self.passwordTextField, self.loginButton])
    stackView.axis = NSLayoutConstraint.Axis.vertical
    stackView.distribution = UIStackView.Distribution.fillEqually
    stackView.alignment = UIStackView.Alignment.fill
    stackView.spacing = 10.0

    return stackView
  }()

  // MARK: Init
  override init(frame: CGRect) {
    super.init(frame: frame)

    self.addSubview(self.stackView)

    self.backgroundColor = .white

    stackView.snp.makeConstraints { make in
      make.center.equalTo(self).offset(-100.0)
      make.leading.equalTo(self).offset(40.0)
      make.trailing.equalTo(self).offset(-40.0)
    }

    loginTextField.snp.makeConstraints { make in
      make.height.equalTo(self).multipliedBy(0.075)
    }
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
