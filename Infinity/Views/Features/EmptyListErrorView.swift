//
//  EmptyListErrorView.swift
//  Infinity
//
//  Created by FLK on 01/10/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class EmptyListErrorView: ErrorView {
  
  init(actionHandler: @escaping ActionHandler) {
    
    let loginBtn = UIButton(type: .system)
    loginBtn.setTitle("Actualiser", for: UIControl.State.normal)
    loginBtn.layer.borderColor = loginBtn.tintColor.cgColor
    loginBtn.layer.borderWidth = 1.0
    
    super.init(image: UIImage.fontAwesomeIcon(code: "fa-check-circle",
                                              textColor: .lightGray,
                                              size: CGSize(width: 100, height: 100))!,
               title: "Hooray !",
               description: "Circulez.\nIl n'y a plus rien à lire ici.",
               action: loginBtn)
    
    loginBtn.addTarget(self, action: #selector(ErrorView.actionForButton), for: UIControl.Event.touchUpInside)
    self.actionHandler = actionHandler
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
