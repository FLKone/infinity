//
//  SectionTableViewCell.swift
//  Infinity
//
//  Created by FLK on 30/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class SectionTableViewCell: UITableViewCell {

  weak var delegate: UITableViewDelegate?

  // MARK: UI
  let accessoryButton: UIButton = {
    let button = UIButton(type: UIButton.ButtonType.system)
    button.titleLabel?.font = UIFont.fontAwesome(ofSize: 20)
    button.setTitle(String.fontAwesomeIcon(code: "fa-bars"), for: UIControl.State.normal)
    return button
  }()

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: .value1, reuseIdentifier: reuseIdentifier)

    self.accessoryView = accessoryButton

  }

  func configure(with section: Section) {
    self.textLabel?.text = section.name
    accessoryButton.sizeToFit()
    accessoryButton.addTarget(delegate, action: #selector(SectionListViewController.sectionAccessoryTapped), for: UIControl.Event.touchUpInside)

    if delegate == nil {
      self.accessoryView = nil
    }
  }
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

}
