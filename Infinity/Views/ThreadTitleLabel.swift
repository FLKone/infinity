//
//  ThreadTitleLabel.swift
//  Infinity
//
//  Created by FLK on 23/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class ThreadTitleLabel: UILabel {

  override func drawText(in rect: CGRect) {
    let insets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    super.drawText(in: rect.inset(by: insets))
  }
}
